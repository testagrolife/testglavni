****Requirements:
react-native (https://facebook.github.io/react-native/docs/getting-started.html#content)
nodejs (4.2.2)
npm (3.10.7)
Android Studio

Notes:
/android/build.gradle 
set gradle version on 1.2.3: 
    classpath 'com.android.tools.build:gradle:1.2.3'


****Before run
npm install

****Start React packager
react-native start || npm start

****Run
react-native run-android

****BUILD PROD
cd android && ./gradlew assembleRelease
