import React, { Component } from 'react';
import { AppRegistry, Text } from 'react-native';

import App from './App/app';
import RequiresConnection from 'react-native-offline-mode';

const offlineFunction = () => <Text>Oh noes, you are out of internetz!</Text>
const offlineComponent = <Text>Oh noes, you are out of internetz!</Text>

class Agrolife extends Component {

    render() {
        return (
            <App />

        );
    }
}

AppRegistry.registerComponent('Agrolife', () => Agrolife);
//AppRegistry.registerComponent('Agrolife', () => RequiresConnection(Agrolife, offlineComponent));