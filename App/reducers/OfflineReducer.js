import immutable from 'seamless-immutable';
import { Type as OfflineActionType } from '../actions/OfflineActions';

const INITIAL_STATE = immutable({
    status: true,
});

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case OfflineActionType.CHANGE_CONNECTION_STATUS:
            let status = action.status;
            return state.merge({status});
            break;

        default:
            return state;
    }
}