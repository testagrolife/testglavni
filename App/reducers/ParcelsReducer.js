import immutable from 'seamless-immutable';
import { Type as ParcelsActionType } from '../actions/ParcelsActions';
import { Type as AppActionType } from '../actions/AppActions';
import { AsyncStorage } from 'react-native';

const INITIAL_STATE = immutable({
    parcels: [],
    parcelsFetching: false,
    parcelsFetchingFailed: false,

    cultureGroups: [],
    cultureGroupsFetching: false,
    cultureGroupsFetchingFailed: false,

    cultures: [],
    culturesFetching: false,
    culturesFetchingFailed: false,

    culturesSaving: false

});

export default function (state = INITIAL_STATE, action) {
    let parcels, parcelsFetching, parcelsFetchingFailed,
        cultureGroups, cultureGroupsFetching, cultureGroupsFetchingFailed,
        cultures, culturesFetching, culturesFetchingFailed, culturesSaving;
    switch (action.type) {
        case AppActionType.LOGOUT:
            return INITIAL_STATE;
            break
//        PARCELS
        case ParcelsActionType.GET_PARCELS_CALL:
            console.log('GET_PARCELS_CALL');
            let parcelsFetching = true;
            return state.merge({parcelsFetching});
            break;
        case ParcelsActionType.GET_PARCELS_SUCCESS:
            console.log('GET_PARCELS_SUCCESS');
            parcels = action.data;
            parcelsFetching = false;
            parcelsFetchingFailed = false;
            return state.merge({parcels, parcelsFetching, parcelsFetchingFailed});
            break;
        case ParcelsActionType.GET_PARCELS_FAILED:
            console.log('GET_PARCELS_FAILED');
            parcels = [];
            parcelsFetching = false;
            parcelsFetchingFailed = true;
            return state.merge({parcels, parcelsFetching, parcelsFetchingFailed});
            break;

//        CULTURE GROUPS
        case ParcelsActionType.GET_CULTURE_GROUPS_CALL:
            console.log('GET_CULTURE_GROUPS_CALL');
            let cultureGroupsFetching = true;
            return state.merge({cultureGroupsFetching});
            break;
        case ParcelsActionType.GET_CULTURE_GROUPS_SUCCESS:
            console.log('GET_CULTURE_GROUPS_SUCCESS');
            cultureGroups = action.data;
            cultureGroupsFetching = false;
            cultureGroupsFetchingFailed = false;
            return state.merge({cultureGroups, cultureGroupsFetching, cultureGroupsFetchingFailed});
            break;
        case ParcelsActionType.GET_CULTURE_GROUPS_FAILED:
            console.log('GET_CULTURE_GROUPS_FAILED');
            cultureGroups = [];
            cultureGroupsFetching = false;
            cultureGroupsFetchingFailed = true;
            return state.merge({cultureGroups, cultureGroupsFetching, cultureGroupsFetchingFailed});
            break;

//        CULTURES
        case ParcelsActionType.GET_CULTURES_CALL:
            console.log('GET_CULTURES_CALL');
            let culturesFetching = true;
            return state.merge({culturesFetching});
            break;
        case ParcelsActionType.GET_CULTURES_SUCCESS:
            console.log('GET_CULTURES_SUCCESS');
            cultures = action.data;
            culturesFetching = false;
            culturesFetchingFailed = false;
            return state.merge({cultures, culturesFetching, culturesFetchingFailed});
            break;
        case ParcelsActionType.GET_CULTURES_FAILED:
            console.log('GET_CULTURES_FAILED');
            cultures = [];
            culturesFetching = false;
            culturesFetchingFailed = true;
            return state.merge({cultures, culturesFetching, culturesFetchingFailed});
            break;

// EDIT CULTURES
        case ParcelsActionType.SET_CULTURES_CALL:
            console.log('SET_CULTURES_CALL');
            culturesSaving = true;
            return state.merge({culturesSaving});
            break;

        case ParcelsActionType.SET_CULTURES_FAILED:
            console.log('SET_CULTURES_FAILED');
            culturesSaving = false;
            return state.merge({culturesSaving});
            break;

        case ParcelsActionType.SET_CULTURES_SUCCESS:
            console.log('SET_CULTURES_SUCCESS');
            culturesSaving = false;
            return state.merge({culturesSaving});
            break;

        default:
            return state;
    }
}