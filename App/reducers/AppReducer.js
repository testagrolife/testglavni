import immutable from 'seamless-immutable';
import { Type as AppActionType } from '../actions/AppActions';
import { AsyncStorage } from 'react-native';
import Moment from 'moment';


const INITIAL_STATE = immutable({
    username: '',
    password: '',
    checkingCredentials: false,
    loggedIn: false,
    logInFailed: false,
    token: null,
    today: new Date(),

    workOrderFetching: false,
    workOrderFetchingFailed: false,
    workOrder: {
        id: null,
        id_status: 1,
        id_radna_operacija: null,
        naziv_radne_operacije: 'Izabrana operacija',
        datum_pocetka: Moment().format('DD-MM-YYYY'),
        datum_zavrsetka: Moment().format('DD-MM-YYYY'),
        materijali: [
//            {
//                id_materijal: null,
//                naziv: '',
//                utroseno: 0,
//                jedinica_mere: ''
//            }
        ],
        pogonske: [
//            {
//                naziv: '',
//                id_pogonska_masina: null
//            }
        ],
        prikljucne: [
//            {
//                naziv: '',
//                id_prikljucna_masina: null
//            }
        ],
        table: [
//            {
//                broj: '',
//                id_tabla: null,
//                naziv: ''
//            }
        ],
        komentar: ''
    },

    workOrders: [],
    workOrdersFetching: false,
    workOrdersFetchingFailed: false,

    workingOperationsGroups: [],
    workingOperationsGroupsFetching: false,
    workingOperationsGroupsFetchingFailed: false,


    workingOperationsForGroupId: [],
    workingOperationsForGroupIdFetching: false,
    workingOperationsForGroupIdFetchingFailed: false,
    workingOperationsGroupId: null,
    workOrderPosting:false,
    workOrderPostingSuccess:false,
    workOrderPostingFailed:false,

    workOrderSelectedTables: [],
    workOrderTables: [],
    workOrderTablesFetching: false,
    workOrderTablesFetchingFailed: false,

    selectedMachines: [],

    machinesGroups: [],
    machinesGroupsFetching: false,
    machinesGroupsFailed: false,


    machinesSubgroups: [],
    machinesSubgroupsFetching: false,
    machinesSubgroupsFetchingFailed: false,

    machines: [],
    machinesFetching: false,
    machinesFetchingFailed: false,

    selectedMaterials: [],

    materialsGroups: [],
    materialsGroupsFetching: false,
    materialsGroupsFetchingFailed: false,

    materialsSubGroups: [],
    materialsSubGroupsFetching: false,
    materialsSubGroupsFetchingFailed: false,

    materials: [],
    materialsFetching: false,
    materialsFetchingFailed: false,

    materialToOrder: {
        id_materijal: null,
        naziv: '',
        utroseno: 0,
        jedinica_mere: ''
    },
    materialQuantity: 1,

    selectedHelpMachines: [],

    helpMachinesGroups: [],
    helpMachinesGroupsFetching: false,
    helpMachinesGroupsFailed: false,

    helpMachines: [],
    helpMachinesFetching: false,
    helpMachinesFetchingFailed: false
});

export default function (state = INITIAL_STATE, action) {
    let workOrder, myObj, workOrderSelectedTables, selectedMachines,
    selectedHelpMachines, selectedMaterials;

    switch (action.type) {
        case AppActionType.LOGOUT:
            return INITIAL_STATE;
            break

        case AppActionType.SET_INITIAL_STATE:
           let componentToSetInitialState = {};
           let value = INITIAL_STATE[action.data];
           componentToSetInitialState[action.data] = value;
           return state.merge({...componentToSetInitialState});
           break;

        // LOGIN

        case AppActionType.SET_TOKEN:
            let tokenSet = action.data;
            return state.merge({token: tokenSet});
            break;

        case AppActionType.LOGIN_REST_CALL:
            console.log('LOGIN_REST_CALL');

            let checkingCredentials = true;
            return state.merge({checkingCredentials});
            break;

        case AppActionType.LOGIN_SUCCESS:
            console.log('LOGIN_SUCCESS' + action.data.token);

            let token = action.data.token;
            AsyncStorage.setItem('token', token);
            let loggedIn = true;
            return state.merge({token, loggedIn});
            break;

        case AppActionType.LOGIN_FAILED:
            console.log('LOGIN_FAILED');

            let logInFailed = true;
            return state.merge({logInFailed});
            break;

        case AppActionType.SET_LOGIN_FAILED_TO_FALSE_AGAIN:
            logInFailed = false;
            return state.merge({logInFailed});
            break;

        // RADNI NALOZI
        case AppActionType.GET_WORK_ORDERS_CALL:
            workOrdersFetching = true;
            return state.merge({workOrdersFetching});
            break;

        case AppActionType.GET_WORK_ORDERS_SUCCESS:
            let workOrders = action.data;
            workOrdersFetching = false;
            return state.merge({workOrders, workOrdersFetching});
            break;

        case AppActionType.GET_WORK_ORDERS_FAILED:
            let workOrdersFetching = false;
            let workOrdersFetchingFailed = true;
            return state.merge({workOrdersFetching, workOrdersFetchingFailed});
            break;

        case AppActionType.GET_WORK_ORDER_BY_ID_CALL:
            workOrderFetching = true;
            return state.merge({workOrderFetching});
            break;

        case AppActionType.GET_WORK_ORDER_BY_ID_SUCCESS:
            workOrder = action.data;
            workOrderFetching = false;
            return state.merge({workOrder, workOrderFetching});
            break;

        case AppActionType.GET_WORK_ORDER_BY_ID_FAILED:
            let workOrderFetching = false;
            let workOrderFetchingFailed = true;
            return state.merge({workOrderFetching, workOrderFetchingFailed});
            break;

        case AppActionType.SEND_WORK_ORDER_CALL:
            workOrderPosting = true;
            return state.merge({workOrderPosting});
            break;

        case AppActionType.SEND_WORK_ORDER_SUCCESS:
            let workOrderPostingSuccess = true;
            workOrderPosting = false;
            return state.merge({workOrder: INITIAL_STATE.workOrder, workOrderPostingSuccess, workOrderPosting});
            break;

        case AppActionType.SEND_WORK_ORDER_FAILED:
            let workOrderPosting = false;
            let workOrderPostingFailed = true;
            return state.merge({workOrderPosting, workOrderPostingFailed});
            break;

        case AppActionType.SET_FOR_WORK_ORDER_SUCCESS_TO_FALSE_AGAIN:
            workOrderPostingSuccess = false;
            return state.merge({workOrderPostingSuccess});
            break;

        case AppActionType.SET_FOR_WORK_ORDER_FAILED_TO_FALSE_AGAIN:
            workOrderPostingFailed = false;
            return state.merge({workOrderPostingFailed});
            break;




        // RADNE OPERACIJE
        case AppActionType.GET_WORKING_OPERATIONS_GROUPS_CALL:
             workingOperationsGroupsFetching = true;
            return state.merge({workingOperationsGroupsFetching});
            break;

        case AppActionType.GET_WORKING_OPERATIONS_GROUPS_SUCCESS:
            let workingOperationsGroups = action.data;
            let workingOperationsGroupsFetching = false;
            return state.merge({workingOperationsGroups, workingOperationsGroupsFetching});
            break;

        case AppActionType.GET_WORKING_OPERATIONS_GROUPS_FAILED:
             workingOperationsGroupsFetching = false;
            let workingOperationsGroupsFetchingFailed = true;
            return state.merge({workingOperationsGroupsFetching, workingOperationsGroupsFetchingFailed});
            break;

        case AppActionType.SET_WORKING_OPERATIONS_GROUP_ID_FOR_FETCH:
            let workingOperationsGroupId = action.data;
            return state.merge({workingOperationsGroupId});
            break;

        case AppActionType.GET_WORKING_OPERATIONS_WITH_GROUP_ID_CALL:
             workingOperationsForGroupIdFetching = true;
            return state.merge({workingOperationsForGroupIdFetching});
            break;

        case AppActionType.GET_WORKING_OPERATIONS_WITH_GROUP_ID_SUCCESS:
            let workingOperationsForGroupId = action.data;

            console.log('A iam in dispatch: action.data: ' + workingOperationsForGroupId);
             workingOperationsForGroupIdFetching = false;
            return state.merge({workingOperationsForGroupId, workingOperationsForGroupIdFetching});
            break;

        case AppActionType.GET_WORKING_OPERATIONS_WITH_GROUP_ID_FAILED:
            let workingOperationsForGroupIdFetching = false;
            let workingOperationsForGroupIdFetchingFailed = true;
            return state.merge({workingOperationsForGroupIdFetching, workingOperationsForGroupIdFetchingFailed});
            break;

        case AppActionType.SET_PROPERTY_FOR_WORKING_OPERATION:
            workOrder = Object.assign({}, state.workOrder);

            workOrder.id_radna_operacija = action.data.id_radna_operacija;
            workOrder.naziv_radne_operacije = action.data.naziv_radne_operacije;

            return state.merge({workOrder});
            break;

        case AppActionType.SET_WORK_ORDER_START_TIME:
            let wo = Object.assign({}, state.workOrder);
            wo.datum_pocetka = action.data.datum_pocetka;
            wo.datum_zavrsetka = action.data.datum_zavrsetka;
            return state.merge({workOrder: wo});
            break;


        case AppActionType.GET_WORK_ORDER_TABLES_SUCCESS:
            let workOrderTables = action.data;
            return state.merge({workOrderTables});
            break;

        case AppActionType.GET_WORK_ORDER_TABLES_CALL:
            let workOrderTablesFetching = true;
            return state.merge({workOrderTablesFetching});
            break;


        case AppActionType.GET_WORK_ORDER_TABLES_FAILED:
            workOrderTablesFetching = false;
            let workOrderTablesFetchingFailed = true;
            return state.merge({workOrderTablesFetching, workOrderTablesFetchingFailed});
            break;

        case AppActionType.SET_SELECTED_TABLE:
            //kopiranje trenutnog state-a i prebacivanje iz objekta u array
            myObj = Object.assign({}, state.workOrderSelectedTables);
            workOrderSelectedTables = [];
            for (let i in myObj) {
                if (myObj.hasOwnProperty(i)) {
                    workOrderSelectedTables.push(myObj[i]);
                }
            }
            // provera da li je korisnik vec odabrao tu tablu za radni nalog
            let isTableAlreadySelected = false;
            for(let i=0;i<workOrderSelectedTables.length;i++){
                if(workOrderSelectedTables[i].id_tabla === action.data.id_tabla){
                    isTableAlreadySelected = true;
                }
            }
            if(isTableAlreadySelected === false){
                workOrderSelectedTables.push(action.data);
            } else {
                //TODO alert vec izabrana
            }
            //merge state-a nazad
            return state.merge({workOrderSelectedTables});

         case AppActionType.SET_SELECTED_TABLES_FROM_WORK_ORDER:
            let workOrderTablesVar = Object.assign({}, state.workOrder);
            if(workOrderTablesVar && workOrderTablesVar.table){
                return state.merge({workOrderSelectedTables: workOrderTablesVar.table});
            }else{
                return state;
            }
            break;

        case AppActionType.REMOVE_SELECTED_TABLE:
            myObj = Object.assign({}, state.workOrderSelectedTables);
            let tableToRemove = action.data;
            workOrderSelectedTables = [];
            for (let i in myObj) {
                if (myObj.hasOwnProperty(i)) {
                    workOrderSelectedTables.push(myObj[i]);
                }
            }
            workOrderSelectedTables.splice(action.data, 1);

            return state.merge({workOrderSelectedTables});
            break;

        case AppActionType.SET_TABLES_FOR_WORK_ORDER:
            workOrder = Object.assign({}, state.workOrder);
            let selectedTables = Object.assign({}, state.workOrderSelectedTables);
            workOrderSelectedTables = [];
            for (let j in selectedTables) {
                if (selectedTables.hasOwnProperty(j)) {
                    workOrderSelectedTables.push(selectedTables[j]);
                }
            }
            workOrder.table = workOrderSelectedTables;
            return state.merge({workOrder: workOrder});

            break;



        ///Machines - Pogonske masine

        case AppActionType.SET_SELECTED_MACHINE:
            myObj = Object.assign({}, state.selectedMachines);
            selectedMachines = [];
            for (let k in myObj) {
                if (myObj.hasOwnProperty(k)) {
                    selectedMachines.push(myObj[k]);
                }
            }
            // provera da li je korisnik vec odabrao tu masinu za radni nalog
            let isMachinesAlreadySelected = false;
            for(let i=0;i<selectedMachines.length;i++){
                if(selectedMachines[i].id_pogonska_masina === action.data.id_pogonska_masina){
                    isMachinesAlreadySelected = true;
                }
            }
            if(isMachinesAlreadySelected === false){
                selectedMachines.push(action.data);
            }
            //merge state-a nazad
            return state.merge({selectedMachines});

        case AppActionType.REMOVE_SELECTED_MACHINE:
            myObj = Object.assign({}, state.selectedMachines);
            selectedMachines = [];
            for (let i in myObj) {
                if (myObj.hasOwnProperty(i)) {
                    selectedMachines.push(myObj[i]);
                }
            }
            selectedMachines.splice(action.data,1);

            return state.merge({selectedMachines});
            break;

        case AppActionType.SET_MACHINES_FOR_WORK_ORDER:
            workOrder = Object.assign({}, state.workOrder);
            selectedMachines = Object.assign({}, state.selectedMachines);
            let workOrderSelectedMachines = [];
            for (let i in selectedMachines) {
                if (selectedMachines.hasOwnProperty(i)) {
                    workOrderSelectedMachines.push(selectedMachines[i]);
                }
            }
            workOrder.pogonske = workOrderSelectedMachines;
            return state.merge({workOrder: workOrder});

            break;

        case AppActionType.SET_SELECTED_MACHINES_FROM_WORK_ORDER:
            let workOrderMachinesVar = Object.assign({}, state.workOrder);
            if(workOrderMachinesVar && workOrderMachinesVar.pogonske){
                return state.merge({selectedMachines: workOrderMachinesVar.pogonske});
            }else{
                return state;
            }
            break;



        case AppActionType.GET_MACHINES_GROUPS_CALL:
            machinesGroupsFetching = true;
            return state.merge({machinesGroupsFetching});
            break;

        case AppActionType.GET_MACHINES_GROUPS_SUCCESS:
            let machinesGroups = action.data;
            let machinesGroupsFetching = false;
            return state.merge({machinesGroups, machinesGroupsFetching});
            break;

        case AppActionType.GET_MACHINES_GROUPS_FAILED:
            machinesGroupsFetching = false;
            let machinesGroupsFailed = true;
            return state.merge({machinesGroupsFetching, machinesGroupsFailed});
            break;

        case AppActionType.GET_MACHINES_SUBGROUPS_CALL:
            machinesSubgroupsFetching = true;
            return state.merge({machinesSubgroupsFetching});
            break;

        case AppActionType.GET_MACHINES_SUBGROUPS_SUCCESS:
            let machinesSubgroups = action.data;
            let machinesSubgroupsFetching = false;
            return state.merge({machinesSubgroups, machinesSubgroupsFetching});
            break;

        case AppActionType.GET_MACHINES_SUBGROUPS_FAILED:
            machinesSubgroupsFetching = false;
            let machinesSubgroupsFetchingFailed = true;
            return state.merge({machinesSubgroupsFetching, machinesSubgroupsFetchingFailed});
            break;

        case AppActionType.GET_MACHINES_CALL:
            machinesFetching = true;
            return state.merge({machinesFetching});
            break;

        case AppActionType.GET_MACHINES_SUCCESS:
            let machines = action.data;
            let machinesFetching = false;
            return state.merge({machines, machinesFetching});
            break;

        case AppActionType.GET_MACHINES_FAILED:
            machinesFetching = false;
            let machinesFetchingFailed = true;
            return state.merge({machinesFetching, machinesFetchingFailed});
            break;


        ///HelpMachines - Prikljucne masine

        case AppActionType.SET_SELECTED_HELP_MACHINE:
            myObj = Object.assign({}, state.selectedHelpMachines);
            selectedHelpMachines = [];
            for (let i in myObj) {
                if (myObj.hasOwnProperty(i)) {
                    selectedHelpMachines.push(myObj[i]);
                }
            }
            // provera da li je korisnik vec odabrao tu masinu za radni nalog
            let isHelpMachinesAlreadySelected = false;
            for(let i=0;i<selectedHelpMachines.length;i++){
                if(selectedHelpMachines[i].id_prikljucna_masina === action.data.id_prikljucna_masina){
                    isHelpMachinesAlreadySelected = true;
                }
            }
            if(isHelpMachinesAlreadySelected === false){
                selectedHelpMachines.push(action.data);
            }
            //merge state-a nazad
            return state.merge({selectedHelpMachines});

        case AppActionType.REMOVE_SELECTED_HELP_MACHINE:
            myObj = Object.assign({}, state.selectedHelpMachines);
            selectedHelpMachines = [];
            for (let i in myObj) {
                if (myObj.hasOwnProperty(i)) {
                    selectedHelpMachines.push(myObj[i]);
                }
            }
            selectedHelpMachines.splice(action.data,1);

            return state.merge({selectedHelpMachines});
            break;

        case AppActionType.SET_HELP_MACHINES_FOR_WORK_ORDER:
            let workO = Object.assign({}, state.workOrder);
            selectedHelpMachines = Object.assign({}, state.selectedHelpMachines);
            let workOrderSelectedHelpMachines = [];
            for (let i in selectedHelpMachines) {
                if (selectedHelpMachines.hasOwnProperty(i)) {
                    workOrderSelectedHelpMachines.push(selectedHelpMachines[i]);
                }
            }
            workO.prikljucne = workOrderSelectedHelpMachines;
            return state.merge({selectedHelpMachines: INITIAL_STATE.selectedHelpMachines, workOrder: workO});

            break;


        case AppActionType.SET_SELECTED_HELP_MACHINES_FROM_WORK_ORDER:
            let workOrderHelpVar = Object.assign({}, state.workOrder);
            if(workOrderHelpVar && workOrderHelpVar.prikljucne){
                return state.merge({selectedHelpMachines: workOrderHelpVar.prikljucne});
            }else{
                return state;
            }
            break;



        case AppActionType.GET_HELP_MACHINES_GROUPS_CALL:
            helpMachinesGroupsFetching = true;
            return state.merge({helpMachinesGroupsFetching});
            break;

        case AppActionType.GET_HELP_MACHINES_GROUPS_SUCCESS:
            let helpMachinesGroups = action.data;
            let helpMachinesGroupsFetching = false;
            return state.merge({helpMachinesGroups, helpMachinesGroupsFetching});
            break;

        case AppActionType.GET_HELP_MACHINES_GROUPS_FAILED:
            helpMachinesGroupsFetching = false;
            let helpMachinesGroupsFailed = true;
            return state.merge({helpMachinesGroupsFetching, helpMachinesGroupsFailed});
            break;


        case AppActionType.GET_HELP_MACHINES_CALL:
            helpMachinesFetching = true;
            return state.merge({helpMachinesFetching});
            break;

        case AppActionType.GET_HELP_MACHINES_SUCCESS:
            let helpMachines = action.data;
            let helpMachinesFetching = false;
            return state.merge({helpMachines, helpMachinesFetching});
            break;

        case AppActionType.GET_HELP_MACHINES_FAILED:
            helpMachinesFetching = false;
            let helpMachinesFetchingFailed = true;
            return state.merge({helpMachinesFetching, helpMachinesFetchingFailed});
            break;


        ///Materials - masine

        case AppActionType.GET_WORK_ORDER_MATERIALS_GROUPS_CALL:
            materialsGroupsFetching = true;
            return state.merge({materialsGroupsFetching});
            break;

        case AppActionType.GET_WORK_ORDER_MATERIALS_GROUPS_SUCCESS:
            let materialsGroups = action.data;
            let materialsGroupsFetching = false;
            return state.merge({materialsGroups, materialsGroupsFetching});
            break;

        case AppActionType.GET_WORK_ORDER_MATERIALS_GROUPS_FAILED:
            materialsGroupsFetching = false;
            let materialsGroupsFetchingFailed = true;
            return state.merge({materialsGroupsFetching, materialsGroupsFetchingFailed});
            break;


        case AppActionType.GET_MATERIALS_SUBGROUPS_CALL:
            materialsSubGroupsFetching = true;
            return state.merge({materialsSubGroupsFetching});
            break;

        case AppActionType.GET_MATERIALS_SUBGROUPS_SUCCESS:
            let materialsSubGroups = action.data;
            let materialsSubGroupsFetching = false;
            return state.merge({materialsSubGroups, materialsSubGroupsFetching});
            break;

        case AppActionType.GET_MATERIALS_SUBGROUPS_FAILED:
            materialsSubGroupsFetching = false;
            let materialsSubGroupsFetchingFailed = true;
            return state.merge({materialsSubGroupsFetching, materialsSubGroupsFetchingFailed});
            break;


        case AppActionType.GET_MATERIALS_CALL:
            materialsFetching = true;
            return state.merge({materialsFetching});
            break;

        case AppActionType.GET_MATERIALS_SUCCESS:
            let materials = action.data;
            let materialsFetching = false;
            return state.merge({materials, materialsFetching});
            break;

        case AppActionType.GET_MATERIALS_FAILED:
            materialsFetching = false;
            let materialsFetchingFailed = true;
            return state.merge({materialsFetching, materialsFetchingFailed});
            break;


        case AppActionType.SET_CHOOSEN_MATERIAL:

            selectedMaterialsS = {};
            if (action.data.forUpdate) {
                selectedMaterialsS = Object.assign({}, state.workOrder.materijali);
            } else {
                selectedMaterialsS = Object.assign({}, state.selectedMaterials);
            }
            selectedMaterials = [];
            for (let i in selectedMaterialsS) {
                if (selectedMaterialsS.hasOwnProperty(i)) {
                    selectedMaterials.push(selectedMaterialsS[i]);
                }
            }
            // provera da li je korisnik vec odabrao tu tablu za radni nalog
            let isMaterialAlreadySelected = false;
            for (let i = 0; i < selectedMaterials.length; i++) {
                if (selectedMaterials[i].id_materijal === action.data.id_materijal) {
                    selectedMaterials[i].utroseno = action.data.utroseno;
                    isMaterialAlreadySelected = true;
                }
            }
            if (isMaterialAlreadySelected === false) {
                selectedMaterials.push(action.data);
            } else {
                //TODO alert vec izabrana
            }
            //merge state-a nazad
            return state.merge({selectedMaterials});
            break;

        case AppActionType.REMOVE_SELECTED_MATERIAL:
            myObj = Object.assign({}, state.selectedMaterials);
            selectedMaterials = [];
            for (let i in myObj) {
                if (myObj.hasOwnProperty(i)) {
                    selectedMaterials.push(myObj[i]);
                }
            }
            selectedMaterials.splice(action.data, 1);

            return state.merge({selectedMaterials});
            break;

        case AppActionType.SET_HELP_MATERIALS_FOR_WORK_ORDER:
            let workOM = Object.assign({}, state.workOrder);
            let selectedMat = Object.assign({}, state.selectedMaterials);
            let workOrderSelectedMaterials = [];
            for (let i in selectedMat) {
                if (selectedMat.hasOwnProperty(i)) {
                    workOrderSelectedMaterials.push(selectedMat[i]);
                }
            }
            workOM.materijali = workOrderSelectedMaterials;
            return state.merge({selectedMaterials: INITIAL_STATE.selectedMaterials, workOrder: workOM});

            break;


        case AppActionType.SET_MATERIALS_FROM_WORK_ORDER:
            let workOrderMats = Object.assign({}, state.workOrder);
            if (workOrderMats && workOrderMats.materijali) {
                return state.merge({selectedMaterials: workOrderMats.materijali});
            } else {
                return state;
            }
            break;

        case AppActionType.SET_STATUS:
            let id_status = action.status;
            console.log('id_status=' + id_status);
            workOrder = state.workOrder.asMutable();
            workOrder.id_status = id_status;
//            state.setIn(["workOrder", "id_status"], id_status);
//            state.merge({workOrder: immutable(workOrder)});
//            state.merge({workOrder});
            return state.merge({workOrder});
            break;

        default:
            return state;
    }
}