/**
 * Created by basskibo on 6.3.17..
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setMaterialsForWorkOrder} from '../actions/AppActions';
import {removeSelectedMaterial, setInitialState, setMaterialsFromWorkOrder} from '../actions/AppActions';
import {Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';


class SelectedMaterials extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.props.selectedMaterials)
        }
    }


    componentWillMount() {
        if (this.props.loadAlreadyChosen === true) {
            this.props.setMaterialsFromWorkOrder();
        }
    }

    componentWillReceiveProps(nextProps) {

        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.selectedMaterials)
        }, function () {
            console.log('Sacekao podatke pre rendera');
            //bez ovoga ne radi...
        });


    }


    renderRow(rowData, sectionID, rowID) {

        return <View style={{flexDirection:'row'}}>
            <TouchableHighlight onPress={this.goToMaterialQuant.bind(this , rowData)}>

                <View style={{flex: 1, marginBottom:20}}>
                    <View style={{width: 300,paddingTop: 15,  alignItems:'flex-start'}}>
                        <Text>{rowData.naziv} </Text>
                    </View>
                </View>
            </TouchableHighlight>
            <TouchableHighlight onPress={this.onPressRow.bind(this , rowID)}
                                style={{width: 100, paddingTop: 10, marginTop:10, alignItems:'flex-end', paddingRight:80, paddingTop:5}}>
                <Icon name="remove" size={26} color="gray"/>
            </TouchableHighlight>
        </View>
    }

    onPressRow(rowID) {
        this.props.removeSelectedMaterial(rowID);
    }

    goBack() {
        this.props.setInitialState('selectedMaterials');
        this.props.navigator.pop();
    }

    goToMaterialGroup() {
        this.props.navigator.push({
            title: 'Grupe materijala',
            component: 'materialGroups',
        });
    }

    goToMaterialQuant(mat) {
        this.props.navigator.push({
            title: 'Podgrupe materijal',
            component: 'materialsQuantity',
            passProps: {material: mat}
        });
    }

    finishAndGoToWorkOrder() {
        this.props.setMaterialsForWorkOrder();
        // this.props.setInitialState('selectedMaterials');
        this.props.navigator.push({
            title: 'Radni nalog',
            component: 'workOrder',
        });
    }


    render() {

        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height
        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);
        const backIcon = (<Icon name="chevron-left" size={23} color="#fff"/>);

        if (this.state.isLoading) {
            return <View><Spinner textStyle={{color: '#FFF'}}/></View>;
            //ovaj spiner ne radi!!!!!!!!!!!!! Ne vidi se nista

        }


        return (
            <View style={styles.container}>
                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                        <View style={{ padding: 13}}>
                            {backIcon}

                        </View>

                    </TouchableHighlight>
                    <View style={{ }}>
                        <Text style={{fontSize: 19, padding: 10, color: '#fff'}}>Lista unetih materijala</Text>


                    </View>
                    <View>
                        <TouchableHighlight onPress={this.finishAndGoToWorkOrder.bind(this)} style={styles.rowElement}>

                            <View style={{ padding: 13}}>
                                {checkIcon}

                            </View>
                        </TouchableHighlight>
                    </View>

                </View>
                <View style={styles.innerContainer}>

                    <ListView
                        style={styles.listView}
                        enableEmptySections={true}
                        dataSource={this.state.dataSource}
                        renderRow={(rowData) => this.renderRow(rowData)}
                        renderSeparator={(sectionId, rowId) =>
                         <View key={rowId} style={styles.separator}/>}
                    />


                    <View style={styles.addButton}>

                        <Icon.Button name="plus-circle"
                                     backgroundColor="#61a40e"
                                     onPress={ this.goToMaterialGroup.bind(this) }
                                     size={30}
                                     padding={20}
                                     marginLeft={10}
                                     marginRight={10}
                                     borderRadius={7}
                                     style={styles.addButton}
                                     iconStyle={{marginRight: 20, marginRight:20 }}
                                     fontSize={8}>
                            <Text style={{ fontSize: 20, color: 'white'}}>
                                Dodaj materijal
                            </Text>
                        </Icon.Button>
                    </View>


                </View>

            </View>
        );

    }
}

function mapStateToProps(state) {

    return {
        selectedMaterials: state.appReducer.selectedMaterials
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setInitialState: (component) => dispatch(setInitialState(component)),
        setMaterialsForWorkOrder: (materials) => dispatch(setMaterialsForWorkOrder(materials)),
        setMaterialsFromWorkOrder: () => dispatch(setMaterialsFromWorkOrder()),
        removeSelectedMaterial: (selectedMaterial) => dispatch(removeSelectedMaterial(selectedMaterial))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SelectedMaterials);


const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        // flexDirection: 'row',
        flex: 1,
        backgroundColor: '#FFF'
    },
    innerContainer: {
        flex: 1,
        padding: 12
    },
    listView: {
        flex: 1
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20
    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    addButton: {
        marginLeft: 10,
        marginRight: 10
    }

});
