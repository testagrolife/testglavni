/**
 * Created by basskibo on 6.3.17..
 */
/**
 * Created by basskibo on 6.3.17..
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getMaterialsGroups} from '../actions/AppActions';
import {Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Icon from 'react-native-vector-icons/FontAwesome';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../assets/fonts/selection.json';
const IconIcoMoon = createIconSetFromIcoMoon(icoMoonConfig);


class MaterialGroups extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.props.materialsGroups)
        }
    }

    componentWillMount() {
        this.props.getMaterialsGroups();
    }


    componentWillReceiveProps(nextProps) {

        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.materialsGroups)
        }, function () {
            console.log('Sacekao podatke pre rendera');
            //bez ovoga ne radi...
        });


    }


    renderRow(rowData, sectionID, rowID) {
        let defaultIcon = "20";
        let defaultSize = 20;
        if(rowData.ime_ikonice){
            defaultIcon = rowData.ime_ikonice;
        }
        let colors = ['#00e6e6','#261a0d','#123456', '#654321', '#fdecba', '#abcdef'];
        let style = [{
            height: 40,
            width: 40,
            borderRadius: 20,
            paddingTop: 9,
            textAlign: 'center',
            backgroundColor: colors[rowID % colors.length]
        }];

        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
                    <View style={styles.row}>
                        <Text style={style}>
                            <IconIcoMoon name={defaultIcon}  size={defaultSize}  color={'#fff'}>
                            </IconIcoMoon>
                        </Text>
                        <Text style={styles.text}>
                            {rowData.naziv}
                        </Text>
                    </View>
               </TouchableHighlight>
    }

    onPressRow(rowData) {
        console.log('onPressRow: rowData.id: ' + rowData.id);

        this.props.navigator.push({
            title: 'Podgrupe materijal',
            component: 'materialSubgroups',
            passProps: {id: rowData.id}
        });
    }

    goBack() {
        this.props.navigator.pop();
    }

    goToMachinesGroups() {
        this.props.navigator.push({
            title: 'Grupe pogonskih masina',
            component: 'machinesGroups',
        });
    }

    finishAndGoToWorkOrder() {
        this.props.setMachinesForWorkOrder();
        this.props.navigator.push({
            title: 'Radni nalog',
            component: 'workOrder',
        });
    }


    render() {
        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height
        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);
        const backIcon = (<Icon name="chevron-left" size={23} color="#fff"/>);

        return (
            <View style={styles.container}>


                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                        <View style={{ padding: 13}}>
                            {backIcon}

                        </View>
                    </TouchableHighlight>
                    <View style={{ }}>
                        <Text style={{fontSize:19, padding:10,color:'#fff'}}>Grupa materijala</Text>

                    </View>
                    <View>
                        <View style={styles.rowElement}>

                            <View style={{ padding: 13}}>

                            </View>
                        </View>
                    </View>

                </View>

                <ListView
                    style={styles.listView}
                    dataSource={this.state.dataSource}
                    enableEmptySections={true}
                    renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData, sectionID, rowID)}
                    renderSeparator={(sectionId, rowId) =>
                    <View key={rowId} style={styles.separator}/>}
                />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        materialsGroups: state.appReducer.materialsGroups
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getMaterialsGroups: () => dispatch(getMaterialsGroups())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MaterialGroups);


const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        // flexDirection: 'row',
        flex: 1,
        backgroundColor: '#FFF',
    },
    innerContainer: {
        padding: 12,
        flex: 1
    },
    listView: {
        flex: 1
    },
    text: {
        marginLeft: 12,
        fontSize: 15,
        paddingRight: 20,
        paddingTop:9
    },
    row: {
        flex:1,
        flexDirection: 'row',
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    addButton: {
        marginLeft: 10,
        marginRight: 10
    }

});
