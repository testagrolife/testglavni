/**
 * Created by basskibo on 6.3.17..
 */
/**
 * Created by basskibo on 6.3.17..
 */
/**
 * Created by basskibo on 6.3.17..
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getMaterials} from '../actions/AppActions';
import {Alert, StyleSheet, Text, TextInput, View, ListView, Image, TouchableHighlight, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';


class Materials extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.props.materials),
            search: false
        }
    }

    componentWillMount() {
        this.props.getMaterials(this.props.idSubgroup, this.props.idGroup);
    }

    componentWillReceiveProps(nextProps) {

        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.materials)
        }, function () {
            console.log('Sacekao podatke pre rendera');
            //bez ovoga ne radi...
        });


    }


    renderRow(rowData, sectionID, rowID) {

        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
            <View style={{flex: 1, flexDirection: 'row', marginBottom:20}}>
                <View style={{width: 300,paddingTop: 15,  alignItems:'flex-start'}}>
                    <Text style={styles.row}>{rowData.naziv} </Text>
                </View>
            </View>

        </TouchableHighlight>
    }

    onPressRow(rowData) {
        this.props.navigator.push({
            title: 'Podgrupe materijal',
            component: 'materialsQuantity',
            passProps: {material: rowData}
        });
    }

    goBack() {
        this.props.navigator.pop();
    }

    goToMachinesGroups() {
        this.props.navigator.push({
            title: 'Grupe pogonskih masina',
            component: 'machinesGroups',
        });
    }

    finishAndGoToWorkOrder() {
        this.props.setMachinesForWorkOrder();
        this.props.navigator.push({
            title: 'Radni nalog',
            component: 'workOrder',
        });
    }
    applyFilter(event) {
        let searchText = event.nativeEvent.text;
        let filteredData = [];
        this.props.materials.forEach(function(material){
           if (material.naziv.toLowerCase().includes(searchText.toLowerCase())) {
                filteredData.push(material)
           }
        });

        this.setState({dataSource: this.state.dataSource.cloneWithRows(filteredData)}, function () {
            console.log('Materials: setovan  novi state');
        })
    }

    filterNotes(searchText, notes) {

        let text = searchText.toLowerCase();

        return filter(notes, (n) => {
            let note = n.body.toLowerCase();
            return note.search(text) !== -1;
      });
    }

    toggleBar() {
        this.setState({
            search: !this.state.search
        });

    }



    render() {

        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height

        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);
        const searchIcon = (<Icon name="search" size={23} color="#fff"/>);
        const closeIcon = (<Icon name="close" size={23} color="#fff"/>);
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>);
        if (this.state.isLoading) {
            return <View><Spinner textStyle={{color: '#FFF'}}/></View>;
            //ovaj spiner ne radi!!!!!!!!!!!!! Ne vidi se nista

        }


        return (
            <View style={styles.container}>
                {/*/>*/}

                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                        <View style={{ padding: 13}}>
                            {backIcon}

                        </View>
                    </TouchableHighlight>
                    <View style={{ }}>
                        { this.state.search ? <TextInput
                                style={styles.searchBar}
                                value={this.state.searchText}
                                onChange={this.applyFilter.bind(this)}
                                placeholder='Traži...'/> :
                            <Text style={{fontSize: 19, padding: 10, color: '#fff'}}>Materijali</Text>
                        }
                    </View>
                    <View>
                        <TouchableHighlight onPress={this.toggleBar.bind(this)} style={styles.rowElement}>

                            <View style={{ padding: 13}}>

                                {this.state.search ? closeIcon : searchIcon}
                            </View>
                        </TouchableHighlight>
                    </View>

                </View>
                <View style={styles.innerContainer}>


                 <ListView
                        style={styles.listView}
                        enableEmptySections={true}
                        dataSource={this.state.dataSource}
                        renderRow={(rowData) => this.renderRow(rowData)}
                        renderSeparator={(sectionId, rowId) =>
                         <View key={rowId} style={styles.separator}/>}
                    />


                </View>

            </View>
        );

    }
}

function mapStateToProps(state) {

    return {
        materials: state.appReducer.materials
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getMaterials: (idGroup, idSubGroup) => dispatch(getMaterials(idGroup, idSubGroup))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Materials);


const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flex: 1
    },
    innerContainer: {
        padding: 12,
        flex: 1,
        marginBottom: 0
    },
    listView: {
        flex: 1
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20
    },
    row: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row' ,
        padding: 5,
        fontSize: 15

    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    addButton: {
        marginLeft: 10,
        marginRight: 10
    },
    searchBar: {
        margin: 3,
        paddingLeft: 10,
        paddingTop: 10,
        fontSize: 20,
        color: "#fff",
        width: 250,
        borderWidth: 1,
        flex: .1,
        borderColor: '#E4E4E4',
    }

});
