import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loginRestCall} from '../actions/AppActions';
import {Alert, BackAndroid, StyleSheet, Text, View, TextInput, Image, Navigator, TouchableHighlight} from 'react-native';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navigation-bar';
import Icon from 'react-native-vector-icons/FontAwesome';
import Menu from '../components/Menu';
import NavBar from '../components/utils/NavBar';
import Drawer from 'react-native-drawer';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../assets/fonts/selection.json';
const  IconIcoMoon = createIconSetFromIcoMoon(icoMoonConfig);

class HomeScreen extends Component {

    constructor() {
        super();
        this.state = {
            menuIsOpen: false
        }
    }

    componentWillMount() {
        this.setState({menuIsOpen: false})
        BackAndroid.addEventListener("hardwareBackPress", () => {
                console.log('dont exit app');
                return true // do not exit app
        })
    }

    componentWillUnmount () {
        this._drawer.close();
        BackAndroid.removeEventListener('hardwareBackPress', this.androidBackHandler);
    }

    gotoWorkOrders() {
        this.props.navigator.push({title: 'Lista radnih naloga', component: 'workOrders'});
    }

    gotoNewWorkOrder() {
        this.props.navigator.push({title: 'Grupa radnih operacija', component: 'workingOperationsGroups'});
    }

    closeControlPanel = () => {
        this._drawer.close()
    };

    openControlPanel = () => {
       this._drawer.open()
    };

    render() {
        const menuIcon = <Icon name="bars" size={23} color="#fff"/>;
        return (
            <Drawer ref={(ref) => this._drawer = ref}
                    type="overlay"
                    content={<Menu navigator={this.props.navigator}
                                   closeControlPanel={this.closeControlPanel}/>}
                    tapToClose={true}
                    openDrawerOffset={0.2} // 20% gap on the right side of drawer
                    panCloseMask={0.2}
                    closedDrawerOffset={-3}
                    styles={drawerStyles}
                    tweenHandler={(ratio) => ({main: {opacity: (2-ratio)/2 } })} >

                <View style={styles.mainContainer}>


                    <View
                        style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                        <TouchableHighlight onPress={this.openControlPanel.bind(this)} style={styles.rowElement}>

                            <View style={{ padding: 13}}>
                                {menuIcon}

                            </View>
                        </TouchableHighlight>
                        <View style={{ }}>
                            <IconIcoMoon name={'1'}
                                         font="icomoon"
                                         style={{fontSize:100,paddingTop:-25 ,paddingRight:10, color:'#fff'}}/>

                        </View>
                        <View style={{ padding: 10}}>

                        </View>


                    </View>

                    <View style={styles.container}>
                        <View style={{ width: 300}}>

                            <Image source={{uri: 'http://www.greensoft.co/images/logo-agrolife.png'}}
                                   style={styles.photo}
                                   resizeMode={'contain'}/>


                            <IconIcoMoon.Button name="3"
                                        backgroundColor="#61a40e"
                                        onPress={ this.gotoNewWorkOrder.bind(this) }
                                        size={40}
                                        padding={20}
                                        borderRadius={12}
                                        fontSize={8}>

                               <Text style={{ fontSize: 20, color: 'white'}}>
                                   Dodaj radni nalog
                               </Text>
                          </IconIcoMoon.Button>

                        </View>

                        <View style={{marginTop: 40, width: 300}}>
                            <IconIcoMoon.Button name="4"
                                         backgroundColor="#61a40e"
                                         onPress={ this.gotoWorkOrders.bind(this) }
                                         size={40}
                                         padding={20}
                                         borderRadius={12}
                                         fontSize={8}>

                                <Text style={{ fontSize: 20, color: 'white'}}>
                                    Lista radnih naloga
                                </Text>
                            </IconIcoMoon.Button>
                        </View>
                    </View>
                </View>
            </Drawer>
        );
    }
}

configureScene = (route) => {
    switch (route.component) {
        case 'homeScreen':
            return Navigator.SceneConfigs.PushFromRight;
        default:
            return Navigator.SceneConfigs.FadeAndroid;
    }
}

function mapStateToProps(state) {
    return {
        logInFailed: state.appReducer.logInFailed
    };
}

function mapDispatchToProps(dispatch) {
    return {
        loginRestCall: (username, password) => dispatch(loginRestCall(username, password))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeScreen);

const drawerStyles = {
  drawer: {shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3},
  main:   {paddingLeft: 3},
};

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFF',
        flex: 1
    },
    container: {
        alignItems: 'center',
    },
    inputLogin: {
        backgroundColor: '#61a40e',
        color: '#fff',
        marginTop: 25,
        width: 290,
        fontSize: 14,
        borderColor: 'transparent',
        paddingLeft: 15,
        height: 50
    },
    marginOnTop: {
        marginTop: 100
    },
    username: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    password: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    loginButton: {
        backgroundColor: "#61a40e",
        width: 100,
        height: 100
    },
    loginImage: {
        width: 250,
        height: 110
    },
    forgotPassword: {
        fontSize: 15,
        color: '#999',
        marginTop: 25,
        marginBottom: 25
    },
    photo: {
        width: 290,
        height: 150,
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center'
        // flex: 0.4
    }
});
