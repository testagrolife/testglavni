/**
 * Created by ernesthemingvej94 on 3/5/17.
 */
/**
 * Created by basskibo on 28.2.17..
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setMachinesForWorkOrder,setSelectedMachinesFromWorkOrder} from '../actions/AppActions';
import {removeSelectedMachine, setInitialState} from '../actions/AppActions';
import {Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';


class SelectedMachines extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.props.selectedMachines)
        }
    }

     componentWillMount(){
      if(this.props.loadAlreadyChosen === true){
        this.props.setSelectedMachinesFromWorkOrder();
      }

    }


    componentWillReceiveProps(nextProps) {

        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.selectedMachines)
        }, function () {
            console.log('Sacekao podatke pre rendera');
            //bez ovoga ne radi...
        });


    }


    renderRow(rowData, sectionID, rowID) {

        return <View>
            <View style={{flex: 1, flexDirection: 'row', marginBottom:20}}>
                <View style={{width: 300,paddingTop: 15,  alignItems:'flex-start'}}>
                    <Text>{rowData.naziv} </Text>
                </View>

                <TouchableHighlight onPress={this.onPressRow.bind(this , rowID)}
                                    style={{width: 100, paddingTop: 10, marginTop:10, alignItems:'flex-end', paddingRight:80, paddingTop:5}}>
                    <Icon name="remove" size={26} color="gray"/>
                </TouchableHighlight>
            </View>

        </View>
    }

    onPressRow(rowID) {
       this.props.removeSelectedMachine(rowID);
    }

    goBack() {
        this.props.setInitialState('selectedMachines');
        this.props.navigator.push({
            title: 'Radni nalog',
            component: 'workOrder',
        });
    }

    goToMachinesGroups() {
        this.props.navigator.push({
            title: 'Grupe pogonskih masina',
            component: 'machinesGroups',
        });
    }

    finishAndGoToWorkOrder(){
        this.props.setMachinesForWorkOrder();
        this.props.setInitialState('selectedMachines');
        this.props.navigator.push({
            title: 'Radni nalog',
            component: 'workOrder',
        });
    }


    render() {

        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height
        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>);

        if (this.state.isLoading) {
            return <View><Spinner textStyle={{color: '#FFF'}}/></View>;
            //ovaj spiner ne radi!!!!!!!!!!!!! Ne vidi se nista

        }


        return (
            <View style={styles.container}>

                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                        <View style={{ padding: 13}}>
                            {backIcon}

                        </View>

                    </TouchableHighlight>
                    <View style={{ }}>
                        <Text style={{fontSize:19, padding:10,color:'#fff'}}>Pogonske mašine</Text>


                    </View>
                    <View>
                        <TouchableHighlight onPress={this.finishAndGoToWorkOrder.bind(this)} style={styles.rowElement}>

                            <View style={{ padding: 13}}>
                                {checkIcon}

                            </View>
                        </TouchableHighlight>
                    </View>

                </View>
                <View style={styles.innerContainer}>

                    <ListView
                        style={styles.listView}
                        enableEmptySections={true}
                        dataSource={this.state.dataSource}
                        renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData,sectionID, rowID)}
                        renderSeparator={(sectionId, rowId) =>
                         <View key={rowId} style={styles.separator}/>}
                    />


                    <View style={styles.addButton}>

                        <Icon.Button name="plus-circle"
                                     backgroundColor="#61a40e"
                                     onPress={ this.goToMachinesGroups.bind(this) }
                                     size={30}
                                     padding={20}
                                     marginLeft={10}
                                     marginRight={10}
                                     borderRadius={7}
                                     style={styles.addButton}
                                     iconStyle={{marginRight: 20, marginRight:20 }}
                                     fontSize={8}>
                            <Text style={{ fontSize: 20, color: 'white'}}>
                                Dodaj pogonsku mašinu
                            </Text>
                        </Icon.Button>
                    </View>


                </View>

            </View>
        );

    }
}

function mapStateToProps(state) {

    return {
        selectedMachines: state.appReducer.selectedMachines
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setInitialState: (component) => dispatch(setInitialState(component)),
        setMachinesForWorkOrder: (machinesArray) => dispatch(setMachinesForWorkOrder(machinesArray)),
        removeSelectedMachine: (selectedMachine) => dispatch(removeSelectedMachine(selectedMachine)),
        setSelectedMachinesFromWorkOrder: () => dispatch(setSelectedMachinesFromWorkOrder())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SelectedMachines);


const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flex: 1,
    },
    innerContainer: {
        padding: 12,
        flex: 1
    },
    listView: {
        flex: 1
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20
    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    addButton: {
        marginLeft: 10,
        marginRight: 10
    }

});
