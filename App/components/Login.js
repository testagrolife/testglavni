import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loginRestCall, setLoginFailedToFalseAgain} from '../actions/AppActions';
import {
    Alert, BackAndroid, StyleSheet, Text, View, TextInput,
    Image, Navigator, TouchableHighlight
} from 'react-native';
import Button from 'react-native-button';
import NavigationBar from 'react-native-navigation-bar';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../assets/fonts/selection.json';
const IconIcoMoon = createIconSetFromIcoMoon(icoMoonConfig);

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            placeholderEmail: 'Email *',
            placeholderSifra: 'Šifra *'
        }
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.token) {
            nextProps.navigator.replace({
                title: 'Radna tabla',
                component: 'home',
                sceneConfig: Navigator.SceneConfigs.FloatFromBottom,
                gestures: null
            });
        } else if (nextProps.logInFailed === true) {
            Alert.alert('Nevalidni podaci!',
                'Pogrešan email ili šifra. Pokušajte ponovo.',
                [
                    {
                        text: 'OK',
                        onPress: () => this.props.setLoginFailedToFalseAgain()
                    },
                ]
            );
        }
    }

    loginPressed() {
        if (this.state.username && this.state.password) {
            this.props.loginRestCall(this.state.username, this.state.password)
        } else {
            Alert.alert('Nevalidni podaci!', 'Email i šifra ne mogu biti prazni.');
        }
    }

    render() {
        return (
            <View style={styles.mainContainer}>


                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <View style={{ padding: 10}}>

                    </View>
                    <View>

                        <IconIcoMoon name={'1'}
                                     font="icomoon"
                                     style={{fontSize:120,paddingTop:-30 ,paddingRight:10, color:'#fff'}}/>
                    </View>

                    <View style={{ padding: 10}}>

                    </View>

                </View>
                <View style={{alignItems:'center'}}>

                    <Text style={styles.marginOnTop}></Text>
                    <TextInput ref={(el) => { this.username = el; }}
                               placeholder={this.state.placeholderEmail}
                               onChangeText={(username) => this.setState({username}) }
                               keyboardType='email-address'
                               style={styles.inputLogin }
                               placeholderTextColor='#FFF'
                               onFocus={() => this.setState({placeholderEmail: ""}) }
                               underlineColorAndroid='transparent'
                    />

                    <TextInput ref={(el) => { this.password = el; }}
                               placeholder={this.state.placeholderSifra}
                               onChangeText={(password) => this.setState({password})}
                               secureTextEntry={true}
                               style={styles.inputLogin}
                               placeholderTextColor='#FFF'
                               onFocus={() => this.setState({placeholderSifra: ""}) }
                               underlineColorAndroid='transparent'
                    />

                    <TouchableHighlight
                        onPress={() => Alert.alert('Promena šifre!', 'Obratite se tehničkoj službi za promenu šifre.')}>
                        <Text style={styles.forgotPassword}>
                            Zaboravili ste šifru?
                        </Text>
                    </TouchableHighlight>

                    <Button
                        style={{fontSize: 25,
                    color: 'white',
                    backgroundColor:'#61a40e',
                    width:290,
                    height: 70,
                    paddingTop: 20 }}
                        styleDisabled={{color: 'red'}}
                        onPress={() => this.loginPressed()}
                    >
                        ULOGUJ SE
                    </Button>
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        logInFailed: state.appReducer.logInFailed,
        token: state.appReducer.token
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setLoginFailedToFalseAgain: () => dispatch(setLoginFailedToFalseAgain()),
        loginRestCall: (username, password) => dispatch(loginRestCall(username, password))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);


const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFF',
        flex: 1
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    inputLogin: {
        backgroundColor: '#61a40e',
        color: '#fff',
        marginTop: 25,
        width: 290,
        fontSize: 14,
        borderColor: 'transparent',
        paddingLeft: 15,
        height: 50
    },
    marginOnTop: {
        marginTop: 100
    },
    username: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    password: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    loginButton: {
        backgroundColor: "#61a40e",
        width: 100,
        height: 100
    },
    loginImage: {
        width: 250,
        height: 110
    },
    forgotPassword: {
        fontSize: 15,
        color: '#999',
        marginTop: 25,
        marginBottom: 25
    }
});
