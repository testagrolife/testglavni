import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getHelpMachines} from '../actions/AppActions';
import {setSelectedHelpMachine} from '../actions/AppActions';
import {Alert, StyleSheet, TextInput, Text, View, ListView, Image, TouchableHighlight, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';


class HelpMachines extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            isLoading: true,
            dataSource: ds.cloneWithRows(this.props.helpMachines),
            search: false
        }
    }

    componentWillMount() {

        if (this.props.id) {
            this.props.getHelpMachines(this.props.id);
        }

    }

    componentWillReceiveProps(nextProps){

        this.setState({
            isLoading: false,
            dataSource: this.state.dataSource.cloneWithRows(nextProps.helpMachines)
        },function () {
            console.log('Sacekao podatke pre rendera');
            //bez ovoga ne radi...
        });
    }


    renderRow(rowData, sectionID, rowID) {
        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
            <View style={styles.row}>
                <Text style={styles.text}>
                    {rowData.naziv}
                </Text>
            </View>
        </TouchableHighlight>
    }

    onPressRow(rowData) {
        console.log('onPressRow: rowData.id: ' + rowData.id);
        let selectedHelpMachine = {id_prikljucna_masina: rowData.id, naziv: rowData.naziv};
        this.props.setSelectedHelpMachine(selectedHelpMachine);
        this.props.navigator.push({
            title: 'Novi radni nalog',
            component: 'selectedHelpMachines'
        });
    }

    goBack() {
        this.props.navigator.pop();
    }

    applyFilter(event) {
        let searchText = event.nativeEvent.text;
        let filteredData = [];
        this.props.helpMachines.forEach(function(helpMachine){
           if (helpMachine.naziv.toLowerCase().includes(searchText.toLowerCase())) {
                filteredData.push(helpMachine)
           }
        });

        this.setState({dataSource: this.state.dataSource.cloneWithRows(filteredData)}, function () {
            console.log('HelpMachines: setovan  novi state');
        })
    }

    filterNotes(searchText, notes) {

        let text = searchText.toLowerCase();

        return filter(notes, (n) => {
            let note = n.body.toLowerCase();
            return note.search(text) !== -1;
      });
    }

    toggleBar() {
        this.setState({
            search: !this.state.search
        });

    }

    render() {

        const searchIcon = (<Icon name="search" size={23} color="#fff"/>);
        const closeIcon = (<Icon name="close" size={23} color="#fff"/>);
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>);
        if (this.state.isLoading) {
            return <View><Spinner  textStyle={{color: '#FFF'}} /></View>;
            //ovaj spiner ne radi!!!!!!!!!!!!! Ne vidi se nista

        }

        return (
            <View style={styles.container}>
                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                        <View style={{ padding: 13}}>
                            {backIcon}

                        </View>
                    </TouchableHighlight>
                    <View style={{ }}>
                        { this.state.search ? <TextInput
                                style={styles.searchBar}
                                value={this.state.searchText}
                                onChange={this.applyFilter.bind(this)}
                                placeholder='Traži...'/> :
                            <Text style={{fontSize:19, padding:10,color:'#fff'}}>Priključne mašine</Text> }
                    </View>
                    <View>
                        <TouchableHighlight onPress={this.toggleBar.bind(this)} style={styles.rowElement}>

                            <View style={{ padding: 13}}>

                                {this.state.search ? closeIcon : searchIcon}
                            </View>
                        </TouchableHighlight>
                    </View>

                </View>

                <View style={styles.innerContainer}>

                    <ListView
                        style={styles.listView}
                        enableEmptySections={true}
                        dataSource={this.state.dataSource}
                        renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData,sectionID, rowID)}
                        renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator}/>}
                    />
                </View>
            </View>
        );

    }
}

function mapStateToProps(state) {

    return {
        loggedIn: state.appReducer.loggedIn,
        helpMachines: state.appReducer.helpMachines,
        helpMachinesFetching: state.appReducer.helpMachinesFetching,
        helpMachinesFetchingFailed: state.appReducer.helpMachinesFetchingFailed
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setSelectedHelpMachine: (selectedHelpMachine) => dispatch(setSelectedHelpMachine(selectedHelpMachine)),
        getHelpMachines: (id) => dispatch(getHelpMachines(id)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HelpMachines);



const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flex: 1,
    },
    innerContainer: {
        padding: 5,
        flex:1
    },
    listView: {
        flex:1
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20
    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    searchBar: {
        margin: 3,
        paddingLeft: 10,
        paddingTop: 10,
        fontSize: 20,
        color: "#fff",
        width: 250,
        borderWidth: 1,
        flex: .1,
        borderColor: '#E4E4E4',
    }

});