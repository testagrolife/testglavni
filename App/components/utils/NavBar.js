import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    Alert, StyleSheet, Text, View, TextInput, Image, Navigator,
    TouchableHighlight, AsyncStorage
} from 'react-native';
import Button from 'react-native-button';
import {Divider} from 'react-native-material-design';
import Icon from 'react-native-vector-icons/FontAwesome';

import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../../assets/fonts/selection.json';
const IconIcoMoon = createIconSetFromIcoMoon(icoMoonConfig);

class NavBar extends Component {

    gotoWorkOrders() {
        this.props.navigator.push({
            title: 'Lista radnih naloga',
            component: 'workOrders'
        });
    }

    gotoParcels() {
        this.props.navigator.push({
            title: 'Lista tabli',
            component: 'parcels'
        });
    }

    logout() {
        this.props.logout();
        AsyncStorage.removeItem("token");
        this.props.navigator.resetTo({
            title: 'Login',
            component: 'login',
            sceneConfig: Navigator.SceneConfigs.FloatFromBottom,
            gestures: null
        });

    }


    render() {
        const menuIcon = (<Icon name="bars" size={23} color="#fff"/>)

        return (
            <View style={{flex:1, flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between'}}>
                <View style={{width: 150, height: 50}}>

                    <TouchableHighlight onPress={this.gotoWorkOrders.bind(this)} style={styles.rowElement}>
                        <Icon name={'chevron-left'}
                              font="icomoon"
                              style={{fontSize: 20, color:'#fff'}}/>
                    </TouchableHighlight>
                </View>

                <View style={{width: 150, height: 50}}>
                    <TouchableHighlight onPress={this.gotoParcels.bind(this)} style={styles.rowElement}>
                        <IconIcoMoon name={'1'}
                                     font="icomoon"
                                     style={{fontSize:20 , color:'#fff'}}/>

                    </TouchableHighlight>
                </View>

                <View style={{width: 150, height: 50, flexDirection:'column'}}>
                    <TouchableHighlight onPress={this.logout.bind(this)} style={styles.rowElement}>
                        <Icon name={'check'}
                              font="icomoon"
                              style={{fontSize: 23, color:'#fff'}}/>
                    </TouchableHighlight>
                </View>

            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        logInFailed: state.appReducer.logInFailed
    };
}

function mapDispatchToProps(dispatch) {
    return {
        loginRestCall: (username, password) => dispatch(loginRestCall(username, password)),
        logout: () => dispatch(logout())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NavBar);


const styles = StyleSheet.create({
    // menu: {
    //     flex: 1,
    //     backgroundColor: '#61a40e',
    // },
    rowElement: {
        height: 100,
        padding: 10
    },
    text: {
        fontSize: 18,
        color: '#FFF',
        marginLeft: 12
    },
    divider: {
        height: 3,
        borderColor: '#FFF',
        backgroundColor: '#FFF'
    }
});
