/**
 * Created by basskibo on 6.3.17..
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getMaterialsSubGroups} from '../actions/AppActions';
import {Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';


class MaterialSubgroups extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.props.materialsSubGroups)
        }
    }


    componentWillMount() {
        if (this.props.id) {
            this.props.getMaterialsSubGroups(this.props.id);
        }
    }


    componentWillReceiveProps(nextProps) {

        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.materialsSubGroups),
        }, function () {

        });
    }


    renderRow(rowData, sectionID, rowID) {

        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
            <View style={{flex: 1, flexDirection: 'row', marginBottom:20}}>
                <View style={{width: 300,paddingTop: 15,  alignItems:'flex-start'}}>
                    <Text style={styles.row}>{rowData.naziv}  </Text>
                </View>

            </View>

        </TouchableHighlight>
    }

    onPressRow(rowData) {
        console.log('MATERIAL === >>  rowData.id: ' + rowData.id);

        this.props.navigator.push({
            title: 'Podgrupe materijal',
            component: 'materials',
            passProps: {idSubgroup: rowData.id, idGroup: this.props.id}
        });
    }
    goBack() {
        this.props.navigator.pop();
    }

    goToMachinesGroups() {
        this.props.navigator.push({
            title: 'Grupe pogonskih masina',
            component: 'machinesGroups',
        });
    }

    finishAndGoToWorkOrder() {
        this.props.setMachinesForWorkOrder();
        this.props.navigator.push({
            title: 'Radni nalog',
            component: 'workOrder',
        });
    }


    render() {

        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height
        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);
        const backIcon = (<Icon name="chevron-left" size={23} color="#fff"/>);

        return (
            <View style={styles.container}>
                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                        <View style={{ padding: 13}}>
                            {backIcon}

                        </View>
                    </TouchableHighlight>
                    <View style={{ }}>
                        <Text style={{fontSize:19, padding:10,color:'#fff'}}>Podgrupa materijala</Text>

                    </View>
                    <View>
                        <View style={styles.rowElement}>

                            <View style={{ padding: 13}}>

                            </View>
                        </View>
                    </View>

                </View>
                <View style={styles.innerContainer}>

                    <ListView
                        style={styles.listView}
                        enableEmptySections={true}
                        dataSource={this.state.dataSource}
                        renderRow={(rowData) => this.renderRow(rowData)}
                        renderSeparator={(sectionId, rowId) =>
                         <View key={rowId} style={styles.separator}/>}
                    />


                </View>

            </View>
        );

    }
}

function mapStateToProps(state) {

    return {
        materialsSubGroups: state.appReducer.materialsSubGroups
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getMaterialsSubGroups: (id) => dispatch(getMaterialsSubGroups(id)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MaterialSubgroups);


const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flex: 1,
        backgroundColor: '#FFF',
    },
    innerContainer: {
        padding: 12,
        flex: 1,
        marginBottom: 30
    },
    listView: {
        flex: 1
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20
    },
    row: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row' ,
        padding: 5,
        fontSize: 15

    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    addButton: {
        marginLeft: 10,
        marginRight: 10
    }

});
