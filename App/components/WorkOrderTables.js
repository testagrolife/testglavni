import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setSelectedTable} from '../actions/AppActions';
import {setWorkingOperationsGroupId} from '../actions/AppActions';
import {getTables} from '../actions/AppActions';
import {Alert, StyleSheet, TextInput, Text, View, ListView, Image, TouchableHighlight, ScrollView} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Divider} from 'react-native-material-design';

let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class WorkOrderTables extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dataSource: ds.cloneWithRows(this.props.workOrderTables),
            search: false
        };
    }

    componentWillMount() {
        this.props.getTables();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.workOrderTables),
        }, function () {
            console.log('setovan  novi state');
        });
    }

    goBack() {
        this.props.navigator.pop();
    }


    toggleBar() {
        // this.state.search = !this.state.search;
        this.setState({
            search: !this.state.search
        });
        // let srch = [];
        // if(this.state.search){
        //     srch.push(<TextInput
        //         style={styles.searchBar}
        //         value={this.state.searchText}
        //         onChange={this.applyFilter.bind(this)}
        //         placeholder='traži...' />)
        // }else{
        //     srch.push(<Text style={{fontSize:19, padding:10,color:'#fff'}}>Radne Operacije</Text>)
        //
        // }
    }

    renderRow(rowData, sectionID, rowID) {
        let row = rowData.naziv + " " + rowData.povrsina + rowData.kultura;
        let kultura = '';
        if (rowData.id_kultura && rowData.id_kultura.naziv) {
            kultura = rowData.id_kultura.naziv;
        }
        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
            <View style={styles.row}>
                <Text style={styles.row}>{rowData.naziv}</Text>
                <Text style={styles.row}>{rowData.povrsina}</Text>
                <Text style={styles.row}>{kultura}</Text>
            </View>
        </TouchableHighlight>

    }

    applyFilter(event) {
        let searchText = event.nativeEvent.text;
        let filteredData = [];
        this.props.workOrderTables.forEach(function (table) {
            if (table.naziv.toLowerCase().includes(searchText.toLowerCase())) {
                filteredData.push(table)
            }
            if (table.id_kultura && table.id_kultura.naziv && table.id_kultura.naziv.toLowerCase().includes(searchText.toLowerCase())) {
                filteredData.push(table)
            }
        });

        this.setState({dataSource: this.state.dataSource.cloneWithRows(filteredData)}, function () {
            console.log('Parcels: setovan  novi state');
        })
    }

    filterNotes(searchText, notes) {

        let text = searchText.toLowerCase();

        return filter(notes, (n) => {
            let note = n.body.toLowerCase();
            return note.search(text) !== -1;
        });
    }

    onPressRow(rowData) {
        let selectedTable = {id_tabla: rowData.id, naziv: rowData.naziv};
        this.props.setSelectedTable(selectedTable);
        this.props.navigator.push({title: 'Izaberi radnu operaciju', component: 'workOrderSelectedTables'});
    }


    render() {
        const backIcon = (<Icon name="chevron-left" size={23} color="#fff"/>);
        const searchIcon = (<Icon name="search" size={23} color="#fff"/>);
        const closeIcon = (<Icon name="close" size={23} color="#fff"/>);

        let content;
        if (this.props.workOrderTables.length > 0) {
            content =
                <View style={styles.container}>

                    <View
                        style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                        <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                            <View style={{ padding: 13}}>
                                {backIcon}

                            </View>

                        </TouchableHighlight>
                        <View style={{}}>

                            { this.state.search ? <TextInput
                                    style={styles.searchBar}
                                    value={this.state.searchText}
                                    onChange={this.applyFilter.bind(this)}
                                    placeholder='Traži...'/> :
                                <Text style={{fontSize:19, padding:10,color:'#fff'}}>Table</Text> }

                        </View>
                        <View>
                            <TouchableHighlight onPress={this.toggleBar.bind(this)} style={styles.rowElement}>

                                <View style={{ padding: 13}}>
                                    {this.state.search ? closeIcon : searchIcon}
                                </View>
                            </TouchableHighlight>
                        </View>

                    </View>
                    <View style={styles.innerContainer}>

                        <View style={styles.rowHeader}>
                            <Text style={styles.row}>Broj</Text>
                            <Text style={styles.row}>Površina</Text>
                            <Text style={styles.row}>Kultura</Text>
                        </View>

                        <ListView
                            dataSource={this.state.dataSource}
                            enableEmptySections={true}
                            renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData,sectionID, rowID)}
                            renderSeparator={(sectionId, rowId) =>
                                <View key={rowId} style={styles.separator}/>}
                        />


                    </View>
                </View>


        } else {
            content = <View style={styles.container}>
                <NavigationBar title={"Lista radnih naloga"}
                               height={50}
                               titleColor={'#fff'}
                               backgroundColor={'#61a40e'}
                               leftButtonTitle="< Nazad"
                               leftButtonTitleColor={'#fff'}
                               onLeftButtonPress={this.goBack.bind(this)}/>
                <View style={styles.innerContainer}>
                    <Text >Table nisu učitane!</Text>
                </View>
            </View>
        }
        return (
            content
        );
    }
}

function mapStateToProps(state) {
    return {
        workOrderTables: state.appReducer.workOrderTables,
        workOrderTablesFetching: state.appReducer.workOrderTablesFetching,
        workOrderTablesFetchingFailed: state.appReducer.workOrderTablesFetchingFailed
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setSelectedTable: (table) => dispatch(setSelectedTable(table)),
        getTables: () => dispatch(getTables())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WorkOrderTables);


const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flex: 1,
        marginBottom: 50
    },
    innerContainer: {
        // marginTop: 50
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        marginRight: 10,
        padding: 10
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    divider: {
        color: '#999'
    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    searchBar: {
        paddingLeft: 30,
        fontSize: 20,
        height: 20,
        borderWidth: 3,
        flex: .1,
        borderColor: '#E4E4E4',
    },
    rowHeader: {
        marginBottom: 10,
        marginLeft: 0,
        paddingLeft: 10,
        justifyContent: 'space-between',
        flexDirection: 'row',
        borderBottomColor: "#000",
        borderBottomWidth: 3
    },
    searchBar: {
        margin: 3,
        paddingLeft: 10,
        paddingTop: 10,
        fontSize: 20,
        color: "#fff",
        width: 250,
        borderWidth: 1,
        flex: .1,
        borderColor: '#E4E4E4',
    }

});