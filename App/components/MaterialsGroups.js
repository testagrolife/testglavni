/**
 * Created by ernesthemingvej94 on 2/21/17.
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getWorkingOperationsWithGroupId} from '../actions/AppActions';
import {setWorkingOperationForWorkOrder} from '../actions/AppActions';
import {getCurrentRouteParams} from '../actions/AppActions';
import {Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';





class MaterialsGroups extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            isLoading: true,
            dataSource: ds.cloneWithRows(this.props.materialsGroups)
        }
    }

    getCurrentRouteParams(currentRoutes, currentRoute){
        for (let i = 0; i < currentRoutes.length; i++) {
            if (currentRoutes[i].component == currentRoute) {
                if (currentRoutes[i].passProps != undefined) {
                    return currentRoutes[i].passProps;
                    break;
                }
            }
        }

    }

    componentWillMount() {
        if (this.props.id) {
            this.props.getWorkingOperationsWithGroupId(this.props.id);
        }
    }

    componentWillReceiveProps(nextProps){
        console.log('componentWillReceiveProps da li je next jednak ***');
        console.log(nextProps.workingOperationsForGroupId === this.props.workingOperationsForGroupId);
        if(nextProps.workingOperationsForGroupId === this.props.workingOperationsForGroupId){
            return;
        }

        this.setState({
            isLoading: false,
            dataSource: this.state.dataSource.cloneWithRows(nextProps.workingOperationsForGroupId)
        },function () {
            console.log('Sacekao podatke pre rendera');
            //bez ovoga ne radi...
        });
    }

    renderRow(rowData, sectionID, rowID) {

        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
            <View style={styles.row}>
                <Text style={styles.text}>
                    {rowData.naziv}
                </Text>
            </View>
        </TouchableHighlight>
    }

    onPressRow(rowData) {
        console.log('onPressRow: rowData.id: ' + rowData.id);
        this.props.setWorkingOperationForWorkOrder(rowData.id, rowData.naziv);
        this.props.navigator.push({title: 'Novi radni nalog', component: 'workOrder',  passProps: {id_radna_operacija:rowData.id,naziv_radne_operacije:rowData.id}});
    }

    goBack() {
        this.props.navigator.pop();
    }

    render() {
        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height
        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);
        const backIcon = (<Icon name="chevron-left" size={23} color="#fff"/>);

        if (this.state.isLoading) {
            return <View><Spinner  textStyle={{color: '#FFF'}} /></View>;
        }

        return (
            <View style={styles.container}>
                <NavigationBar
                    style={{flex:1}}
                    title={"izaberi radnu operaciju"}
                    height={50}
                    width={width}
                    titleColor={'#fff'}
                    backgroundColor={'#61a40e'}
                    leftButtonTitle="< Nazad"
                    leftButtonTitleColor={'#fff'}
                    onLeftButtonPress={this.goBack.bind(this)}/>

                <View style={styles.innerContainer}>
                    <ListView
                        style={styles.listView}
                        enableEmptySections={true}
                        dataSource={this.state.dataSource}
                        renderRow={(rowData) => this.renderRow(rowData)}
                        renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator}/>}
                    />
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        loggedIn: state.appReducer.loggedIn,
        workingOperationsForGroupId: state.appReducer.workingOperationsForGroupId,
        workingOperationsForGroupIdFetching: state.appReducer.workingOperationsForGroupIdFetching,
        workingOperationsForGroupIdFetchingFailed: state.appReducer.workingOperationsForGroupIdFetchingFailed,
        workingOperationsGroupId: state.appReducer.workingOperationsGroupId
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getWorkingOperationsWithGroupId: (id) => dispatch(getWorkingOperationsWithGroupId(id)),
        getCurrentRouteParams: (currentRoutes, currentRoute) => dispatch(getCurrentRouteParams(currentRoutes, currentRoute)),
        setWorkingOperationForWorkOrder: (id, naziv) => dispatch(setWorkingOperationForWorkOrder(id, naziv))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MaterialsGroups);

const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flexDirection: 'row',
        flex: 1,
        padding: 12
    },
    innerContainer: {
        marginTop: 50,
        flex:1
    },
    listView: {
        flex:1
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20
    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    }

});
