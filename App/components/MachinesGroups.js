import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getMachinesGroups} from '../actions/AppActions';
import {Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../assets/fonts/selection.json';
const IconIcoMoon = createIconSetFromIcoMoon(icoMoonConfig);
import Icon from 'react-native-vector-icons/FontAwesome';





class MachinesGroups extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            isLoading: true,
            dataSource: ds.cloneWithRows(this.props.machinesGroups)
        }
    }

    componentWillMount() {
            this.props.getMachinesGroups();
    }


    componentWillReceiveProps(nextProps){

        this.setState({
            isLoading: false,
            dataSource: this.state.dataSource.cloneWithRows(nextProps.machinesGroups)
        },function () {
            console.log('Sacekao podatke pre rendera');
            //bez ovoga ne radi...
        });



    }


    renderRow(rowData, sectionID, rowID) {

       var defaultIcon = "9";
              var defaultSize = 20;
              if(rowData.ime_ikonice){
               defaultIcon = rowData.ime_ikonice;
              }
              if(rowData.naziv == "Kombajn"){
                  defaultIcon = "10";
                  defaultSize = 30;
              }
           let colors = ['#00e6e6','#261a0d','#123456', '#654321', '#fdecba', '#abcdef'];
           let style = [{
                  height: 40,
                  width: 40,
                  borderRadius: 20,
                  paddingTop: 9,
                  textAlign: 'center',
                  backgroundColor: colors[rowID % colors.length]
                }];
              return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
                  <View style={styles.row}>

                   <Text style={style}>
                     <IconIcoMoon name={defaultIcon}  size={defaultSize}  color={'#fff'}>
                     </IconIcoMoon>
                 </Text>
                      <Text style={styles.text}>
                          {rowData.naziv}
                      </Text>
                  </View>
              </TouchableHighlight>
    }

    onPressRow(rowData) {
        console.log('onPressRow: rowData.id: ' + rowData.id);

        this.props.navigator.push({title: 'Podgrupe pogonskih masina', component: 'machinesSubgroups', passProps: {id: rowData.id, nazivGrupe: rowData.naziv}});
    }

    goBack() {
        this.props.navigator.pop();
    }



    render() {

        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height
//        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>);



        return (
            <View style={styles.container}>
                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>
                        <View style={{ padding: 13}}>
                            {backIcon}
                        </View>
                    </TouchableHighlight>
                    <View style={{ }}>
                        <Text style={{fontSize:19, padding:10,color:'#fff'}}>Podgrupa pogonskih mašina</Text>
                    </View>
                    <View>
                        <View style={styles.rowElement}>
                            <View style={{ padding: 13}}>
                            </View>
                        </View>
                    </View>

                </View>
                <View style={styles.innerContainer}>

                    <ListView
                        style={styles.listView}
                        enableEmptySections={true}
                        dataSource={this.state.dataSource}
                        renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData,sectionID, rowID)}
                        renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator}/>}
                    />


                </View>

            </View>
        );

    }
}

function mapStateToProps(state) {

    return {
        loggedIn: state.appReducer.loggedIn,
        machinesGroups: state.appReducer.machinesGroups,
        machinesGroupsFetching: state.appReducer.machinesGroupsFetching,
        machinesGroupsFailed: state.appReducer.machinesGroupsFailed
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getMachinesGroups: () => dispatch(getMachinesGroups())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MachinesGroups);



const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flex: 1,
    },
    innerContainer: {
        padding: 12,
        flex:1
    },
    listView: {
        flex:1
    },
    text: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row' ,
        padding: 5,
        fontSize: 15

    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    }

});
