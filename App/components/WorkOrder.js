import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    setInitialState,
    setWorkOrderFailedToFalseAgain,
    sendWorkOrder,
    setWorkOrderSuccessToFalseAgain,
    setSelectedTablesFromWorkOrder } from '../actions/AppActions';
import {
    Alert,
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    ScrollView,
    TouchableHighlight,
    ListView,
    Image } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import NavigationBar from 'react-native-navigation-bar';
import Moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Divider} from 'react-native-material-design';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../assets/fonts/selection.json';
const IconIcoMoon = createIconSetFromIcoMoon(icoMoonConfig);

class WorkOrder extends Component {

    constructor(props) {
        super(props);
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            workOrder: this.props.workOrder
        }

    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.workOrderPostingFailed === true && this.props.id){
            this.props.setWorkOrderFailedToFalseAgain();
            Alert.alert(
                'Neuspešno dodavanje',
                'Došlo je do greske! Vaš nalog nije uspešno snimljen!',
            );
            return;
        }
        if(nextProps.workOrderPostingSuccess === true && this.props.workOrder.id == null){
            this.props.setWorkOrderSuccessToFalseAgain();
            Alert.alert(
                'Dodat radni nalog!',
                'Uspešno ste dodali radni nalog.',
                [
                    {text: 'OK', onPress: () => this.goToHome()}
                ],
                { cancelable: false }
            );
            return;
        }
        if(nextProps.workOrderPostingSuccess === true && this.props.workOrder.id !== null){
            this.props.setWorkOrderSuccessToFalseAgain();
            Alert.alert(
                'Izmenjen radni nalog!',
                'Uspešno ste izmenili radni nalog.',
                [
                    {text: 'Ok', onPress: () => this.goToHome()}
                ],
                { cancelable: false }
            );
            return;
        }
        this.setState({workOrder: nextProps.workOrder, workOrderPosting: nextProps.workOrderPosting}, function () {
            console.log('WorkOrder: setovan  novi state');
        })
    }

    renderRow(rowData, rowID) {

        return <View style={{flex: 1, flexDirection: 'row', marginBottom:10}}>
            {/*<View style={{alignItems:'flex-end'}}>*/}
            {/*<Text>{rowData.naziv} </Text>*/}
            {/*</View>*/}

        </View>
    }


    goToWorkingOperationsGroups() {
        this.props.setSelectedTablesFromWorkOrder();
        this.props.navigator.push({title: 'Izaberi grupu radne operacije', component: 'workingOperationsGroups'});
    }

    goToParcels() {
        this.props.setSelectedTablesFromWorkOrder();
        this.props.navigator.push({title: 'Izaberi grupu radne operacije', component: 'workOrderSelectedTables'});
    }

    goBack() {
        this.props.setInitialState('workOrder');
        this.props.navigator.push({title: 'Radna tabla', component: 'home'});
    }

    goToWorkOrderFrom() {
        this.props.navigator.push({
            title: 'Izaberi radnu operaciju',
            component: 'workOrderFrom',
            passProps:{
                datumPocetka: this.state.workOrder.datum_pocetka,
                datumZavrsetka: this.state.workOrder.datum_zavrsetka
        }});

    }

    goToSelectedMachines(){
        let load = false;
            if(this.state.workOrder.pogonske.length) {
                load = true;
            }
        this.props.navigator.push({title: 'Lista izabranih pogonskih masina',
                                   component: 'selectedMachines',
                                   passProps:{loadAlreadyChosen: load}});
    }

    goToSelectedMaterial() {
        let load = false;
        if(this.state.workOrder.materijali.length){
           load = true;
        }
        this.props.navigator.push({title: 'Lista izabranih pogonskih masina',
                                   component: 'selectedMaterials' ,
                                   passProps:{loadAlreadyChosen: load}});
    }

    goToSelectedHelpMachines(){
        let load = false;
        if(this.state.workOrder.prikljucne.length){
            load = true;
        }
        this.props.navigator.push({title: 'Lista izabranih prikljucnih masina',
                                   component: 'selectedHelpMachines',
                                   passProps:{loadAlreadyChosen: load}});
    }

    areYouSure(){
        if(this.state.workOrder.id){
            Alert.alert('Izmena radnog naloga!',
                        'Da li ste sigurni da želite da izmenite ovaj radni nalog?',
                        [
                            {text: 'Otkaži', onPress: () => console.log(''), style: 'cancel'},
                            {text: 'Izmeni', onPress: () => this.sendWorkOrder()},
                        ],
                        { cancelable: false }
            );
        } else {
            Alert.alert('Novi radni nalog!',
                        'Da li ste sigurni da želite da sačuvate ovaj radni nalog?',
                        [
                            {text: 'Otkaži', onPress: () => console.log(''), style: 'cancel'},
                            {text: 'Sačuvaj', onPress: () => this.sendWorkOrder()},
                        ],
                        { cancelable: false }
            );
        }
    }

    sendWorkOrder(){
        this.props.sendWorkOrder(this.state.workOrder);

    }

    goToHome(){
        let routes = this.props.navigator.state.routeStack;
        for (let i = routes.length - 1; i >= 0; i--) {
            if(routes[i].component === "home"){
                let destinationRoute = this.props.navigator.getCurrentRoutes()[i];
                this.props.navigator.popToRoute(destinationRoute);
            }
        }
    }

    addAnotherOne(){
        this.props.setInitialState('workOrder');
        this.goToWorkingOperationsGroups();
    }

    goToMaterialQuant(mat) {
        this.props.navigator.push({
            title: 'Podgrupe materijal',
            component: 'materialsQuantity',
            passProps: {material: mat}
        });
    }


    setStateForComment(text){
        let workOrder = Object.assign({}, this.state.workOrder);
        workOrder.komentar = text;
        this.setState({workOrder}, function () {
            console.log('setovan  novi state');
        })

    }

    renderSemaphore(workOrder) {
        let circle;
        switch (workOrder.id_status) {
            case 1:
                circle = <View style={styles.greenCircle} />
                break;
            case 2:
                circle = <View style={styles.yellowCircle} />
                break;
            case 3:
                circle = <View style={styles.redCircle} />
                break;
            case 4:
                circle = <View style={styles.lockedCircle} >
                            <Icon name="key"
                                  size={18}
                                  color="#fff"/>
                         </View>
                break;

            default:
                circle = <View style={styles.greenCircle} />

        }

        return <TouchableHighlight onPress={this.changeStatus.bind(this)} >
                   {circle}
               </TouchableHighlight>
    }

    changeStatus() {
        this.props.navigator.push({title: 'Promeni status',
                                   component: 'setStatus'});

    }

    render() {

        let stringTables = "";
        let workOrderSel = this.props.workOrder.table;
        for (let t in workOrderSel) {
            stringTables += (workOrderSel[t].naziv);
            if (t != workOrderSel.length - 1) {
                stringTables += ' ,';
            }
        }

        let stringMachines = "";
        let machines = this.props.workOrder.pogonske;
        for (let t in machines) {
            stringMachines += (machines[t].naziv);
            if (t != machines.length - 1) {
                stringMachines += ' ,';
            }
        }
        if(stringMachines === ""){
            stringMachines = "Izaberi pogonske mašine";
        }

        let stringHelpMachines = "";
        let helpMachines = this.props.workOrder.prikljucne;
        for (let t in helpMachines) {
            stringHelpMachines += (helpMachines[t].naziv);
            if (t != helpMachines.length - 1) {
                stringHelpMachines += ' ,';
            }
        }
        if(stringHelpMachines === ""){
            stringHelpMachines = "Izaberi priključne mašine";
        }


        const calendarIcon =   (<Icon name="calendar" size={30} color="#61a40e"/>)
        const checkIcon =      (<Icon name="check" size={23} color="#fff"/>)
        const backIcon =       (<Icon name="arrow-left" size={23} color="#fff"/>)
        const tractorIcon =    (<Icon name="cogs" size={23} color="#000"/>)
        const prikljucnaIcon = (<Icon name="car" size={23} color="#000"/>)

        let dateTo = null;
        if( this.props.workOrder.datum_pocetka !== this.props.workOrder.datum_zavrsetka ){
            dateTo = <TouchableHighlight onPress={this.goToWorkOrderFrom.bind(this)} >
                              <Text style={styles.dateLabelTo}>{this.state.workOrder.datum_zavrsetka}</Text>
                        </TouchableHighlight>

        }

        let materialsList = [];
        let mats = this.props.workOrder.materijali;
        for (let i = 0; i < mats; i++) {

            materialsList.push(
                <View key={i}>
                    <View >
                        <Text >i.naziv</Text>
                    </View>
                    <View>
                        <Text>i.jedinica_mere</Text>
                    </View>
                    <View>
                        <Text>i.utroseno</Text>
                    </View>
                </View>
            )
        }
        const semaphore = this.renderSemaphore(this.props.workOrder);

        return (
            <ScrollView style={styles.scrollView}>
                <View style={{ flex: 1, backgroundColor: '#262626'}}>
                    <Spinner
                        visible={this.state.workOrderPosting}
                        textContent={"Loading..."}
                        textStyle={{color: '#61a40e'}} />
                </View>
                <View>
                    <View>

                        <View
                            style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                            <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.menuItem}>

                                <View style={{ padding: 13}}>
                                    {backIcon}

                                </View>

                            </TouchableHighlight>
                            <View style={{ }}>
                                <Text style={{fontSize: 19, padding: 10, color: '#fff'}}>Radni nalog</Text>
                            </View>
                            <TouchableHighlight onPress={this.areYouSure.bind(this)} style={styles.menuItem}>

                                <View style={{ padding: 13}}>
                                    {checkIcon}

                                </View>
                            </TouchableHighlight>

                        </View>
                    </View>

                    <View style={styles.rowElement}>
                        <TouchableHighlight onPress={this.goToWorkingOperationsGroups.bind(this)}>
                            <View style={{flex:1,
                                          flexDirection:'row',
                                          justifyContent: 'space-between'}}>
                                <Text style={styles.underlined1}>
                                    {this.state.workOrder.naziv_radne_operacije}
                                </Text>

                                <View>
                                    {semaphore}
                                </View>

                            </View>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={this.goToParcels.bind(this)}>
                            <View style={{flex:1, flexDirection:'row'}}>
                                <Text style={styles.underlined2}>{stringTables}</Text>
                            </View>
                        </TouchableHighlight>
                    </View>

                    <Divider style={styles.divider}/>

                    <View style={styles.rowElementForDate}>
                    <TouchableHighlight onPress={this.goToWorkOrderFrom.bind(this)} style={styles.rowElement}>

                        <View style={{flexDirection: 'row' ,flex:1}}>
                            <View style={{width: 50}}>
                                {calendarIcon}
                            </View>
                            <Text style={styles.dateLabel}>{this.state.workOrder.datum_pocetka}</Text>
                            {dateTo}
                        </View>

                    </TouchableHighlight>
                    </View>


                    <Divider style={styles.divider}/>

                    <View style={styles.rowElement}>
                        <Text style={styles.subHeader}>Materijal</Text>

                        {materialsList}

                        <View style={styles.materialsLoop}>
                            {
                                this.props.workOrder.materijali.map((item, index) => {
                                    return (
                                        <TouchableHighlight onPress={this.goToMaterialQuant.bind(this, item)}>

                                            <View key={index}
                                                  style={{flex:1,  flexDirection: 'row' ,marginBottom:5,marginLeft:10}}>
                                                <View >
                                                    <Text style={styles.tableRowFirst}>{item.naziv}</Text>
                                                </View>
                                                <View >
                                                    <Text style={styles.tableRow}>{item.jedinica_mere}</Text>
                                                </View>
                                                <View >
                                                    <Text style={styles.tableRowLast}>{item.utroseno}</Text>
                                                </View>
                                            </View>
                                        </TouchableHighlight>
                                    )
                                })
                            }
                        </View>

                        <View style={styles.addButton2}>
                            <Icon.Button name="plus-circle"
                                         backgroundColor="#61a40e"
                                         onPress={ this.goToSelectedMaterial.bind(this) }
                                         size={20}
                                         padding={10}
                                         marginLeft={10}
                                         marginRight={10}
                                         fontSize={7}>
                                <Text style={{ fontSize: 16, color: 'white'}}>
                                    Dodaj materijal
                                </Text>
                            </Icon.Button>
                        </View>

                    </View>

                    <Divider style={styles.divider}/>

                    <TouchableHighlight onPress={this.goToSelectedMachines.bind(this)} style={styles.rowElement}>
                        <View style={{flex:1, flexDirection: 'row'}}>
                            <View style={{width: 50, marginTop: 15, alignItems:'flex-start'}}>
                                <IconIcoMoon name="9" size={30} fontSize={10} color={'#61a40e'}>
                                </IconIcoMoon>
                            </View>
                            <View style={styles.choosenItem}>
                                <Text style={styles.textBox}>{stringMachines}</Text>
                            </View>
                        </View>
                    </TouchableHighlight>


                    <Divider style={styles.divider}/>

                    <TouchableHighlight onPress={this.goToSelectedHelpMachines.bind(this)} style={styles.rowElement}>
                        <View style={{flex:1, flexDirection: 'row'}}>
                            <View style={{width:50, marginTop:10,  alignItems:'flex-start'}}>
                                <IconIcoMoon name="prikolica" size={40} fontSize={10} color={'#61a40e'}>
                                </IconIcoMoon>
                            </View>
                            <View style={styles.choosenItem}>
                                <Text style={styles.textBox}>{stringHelpMachines}</Text>
                            </View>
                        </View>
                    </TouchableHighlight>

                    <Divider style={styles.divider}/>


                    <View style={styles.rowElement}>

                        <View style={{flex:1, flexDirection: 'row'}}>
                            <View style={{width: 50, marginTop: 10, paddingLeft: 5, alignItems:'flex-start'}}>
                                <Text style={{fontSize:15, fontWeight:'bold'}}>
                                    Opis
                                </Text>
                            </View>
                            <View style={styles.choosenItem}>
                                <TextInput style={{color:'#3b3b3b'}}
                                           multiline={true}
                                           numberOfLines={1}
                                           keyboardAppearance={'light'}
                                           defaultValue={this.state.workOrder.komentar}
                                           placeholder="Unesi komentar..."
                                           onChangeText={(text) => this.setStateForComment(text)}/>
                            </View>
                        </View>
                    </View>

                </View>
            </ScrollView>
        );
    }
}


function mapStateToProps(state) {
    return {
        workOrder: state.appReducer.workOrder,
        workOrderFetching : state.appReducer.workOrderFetching,
        workOrderFetchingFailed: state.appReducer.workOrderFetchingFailed,
        workOrderPosting: state.appReducer.workOrderPosting,
        workOrderPostingSuccess : state.appReducer.workOrderPostingSuccess,
        workOrderPostingFailed: state.appReducer.workOrderPostingFailed

};

}

function mapDispatchToProps(dispatch) {
    return {
        setInitialState: (component) => dispatch(setInitialState(component)),
        sendWorkOrder: (w) => dispatch(sendWorkOrder(w)),
        setWorkOrderSuccessToFalseAgain: () => dispatch(setWorkOrderSuccessToFalseAgain()),
        setWorkOrderFailedToFalseAgain: () => dispatch(setWorkOrderFailedToFalseAgain()),
        setSelectedTablesFromWorkOrder: () => dispatch(setSelectedTablesFromWorkOrder())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WorkOrder);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    scrollView: {
        backgroundColor: '#FFF'
    },
    divider: {
        marginBottom: 20,
        marginTop: 10
    },
    choosenItem: {
        borderColor: '#c2c2c2',
        borderWidth: 1,
        margin: 10,
        width: 250
    },
    textBox: {
        padding: 10
    },
    navBar: {
        marginBottom: 50
    },
    photo: {
        width: 100,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    subHeader: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 5
    },
    subHeader2: {
        fontSize: 14,
        fontWeight: 'bold',
        marginBottom: 3
    },
    tableRow: {
        borderWidth: 0.5,
        width: 65,
        padding: 10,
        height: 40,
        borderColor: '#0f0f0f',
        color: '#999',
    },
    tableRowFirst: {
        borderWidth: 0.5,
        width: 180,
        padding: 10,
        height: 40,
        borderColor: '#0f0f0f',
        color: '#999',
        marginLeft: -10
    },
    tableRowLast: {
        borderWidth: 0.5,
        width: 65,
        padding: 10,
        height: 40,
        borderColor: '#0f0f0f',
        color: '#999',
        marginRight: 10
    },

    dotted: {
        borderBottomColor: '#6b6b6b',
        borderBottomWidth: 1,
        padding: 5,
        // borderStyle: 'dashed'
    },
    underlined1: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 10,
        borderBottomColor: '#6b6b6b',
        borderBottomWidth: 1,
        flexWrap: "wrap",
    },
    underlined2: {
        fontSize: 16,
        marginTop: 10,
        marginLeft: 5,
        borderBottomColor: '#6b6b6b',
        borderBottomWidth: 1
    },
    rowElement: {
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 20,
        marginRight: 20
    },
    rowElementCal: {
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 20,
        marginRight: 5
    },
    rowElementForDate: {
        flex:1,
       // flexDirection: 'row',
        marginLeft: 5,
        marginRight: 10
    },
    rowElementLg: {
        marginBottom: 50,
        marginTop: 10
    },
    rowElementDate: {
        flex: 1
    },
    addButton: {
        marginLeft: 10,
        marginRight: 10
    },
    addButton2: {
        width: 200,
        marginRight: 10
    },
    inputLogin: {
        backgroundColor: '#61a40e',
        color: '#fff',
        marginTop: 25,
        width: 290,
        fontSize: 14,
        borderColor: 'transparent',
        paddingLeft: 15,
        height: 50
    },
    marginOnTop: {
        marginTop: 100
    },
    dateLabel:{
        flex:1,
        textAlign: 'center',
        borderColor: '#c2c2c2',
        borderWidth: 1,
        marginLeft: 1,
        padding: 5,
        paddingLeft: 25,
        paddingRight: 25
    },
    dateLabelTo:{
        flex:1,
        textAlign: 'center',
        borderColor: '#c2c2c2',
        borderWidth: 1,
        marginLeft: 5,
        padding: 5,
        paddingLeft: 25,
        paddingRight: 25
    },
    picker: {
        borderColor: '#000',
        paddingLeft: 5,
        color: '#999'
    },
    listViewTable: {
        // alignItem:'flex-end'
    },
    materialsLoop: {
        marginLeft: 0
    },
    redCircle: {
        width: 40,
        height: 40,
        borderRadius: 100/2,
        backgroundColor: 'red',

    },
    greenCircle: {
        width: 40,
        height: 40,
        borderRadius: 100/2,
        backgroundColor: 'green',
    },
    yellowCircle: {
        width: 40,
        height: 40,
        borderRadius: 100/2,
        backgroundColor: 'orange'
    },
    lockedCircle: {
        width: 40,
        height: 40,
        borderRadius: 100/2,
        backgroundColor: 'red',
        padding: 10
    },
    circle: {
        width: 40,
        height: 40,
        borderRadius: 100/2,
    }
});
