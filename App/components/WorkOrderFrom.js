/**
 * Created by basskibo on 27.2.17..
 */
import React, {Component} from 'react';
import DatePicker from 'react-native-datepicker';
import {connect} from 'react-redux';
import {StyleSheet, Text, View, TouchableHighlight} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import {Divider} from 'react-native-material-design';
import Button from 'react-native-button';
import Icon from 'react-native-vector-icons/FontAwesome';
import Moment from 'moment';
import {setStartTimeForWorkingOrder} from '../actions/AppActions';





class WorkOrderFrom extends Component {

    constructor(props) {
        super(props)
        let valueOfRenderDateTo = true;
        if( this.props.datumPocetka === this.props.datumZavrsetka){
            valueOfRenderDateTo = false;
        }
        this.state = {
            dateFrom: this.props.datumPocetka,
            dateTo: this.props.datumZavrsetka,
            renderDateTo: valueOfRenderDateTo
        }

    }


    goBack() {
        this.props.navigator.pop();
    }

    goToWorkOrder() {
        this.props.setStartTimeForWorkingOrder(this.state.dateFrom, this.state.dateTo);
        this.props.navigator.push({title: 'Izaberi radnu operaciju', component: 'workOrder'});

    }

    addAnotherDate() {
        this.setState({dateTo:this.state.dateFrom, renderDateTo: true}, function () {
            console.log('setovan  novi state');
        })
    }


    render() {

        const calendarIcon = (<Icon name="calendar" size={30} color="#61a40e"/>)
        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);
        const backIcon = (<Icon name="chevron-left" size={23} color="#fff"/>);

        let renderMore = null;
        if(this.state.renderDateTo === true){
            renderMore = <View>

                <View >
                    <Text style={{marginTop: 30, paddingLeft:10 }}> Izaberi datum zavrsetka</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <View style={{width: 50, marginTop:15, alignItems:'flex-start'}}>
                        {calendarIcon}
                    </View>
                    <View>
                        <DatePicker
                            style={{width: 310 ,marginTop:20, paddingRight:30}}
                            date={this.state.dateTo}
                            mode="date"
                            placeholder="Izaberi datum"
                            format="DD-MM-YYYY"
                            minDate={this.state.dateFrom}
                            showIcon={false}
                            maxDate="01-06-2025"
                            confirmBtnText="Potvrdi"
                            cancelBtnText="Odustani"

                            onDateChange={(date) => {this.setState({dateTo: date})}}
                        />

                    </View>
                </View>

            </View>


        }else{
            renderMore = <View>
                <Text>
                    Ukoliko niste završili radni nalog u istom danu potrebno je da izaberete datum kada ste završili.
                </Text>
                <Divider style={styles.divider}/>
                <Button
                        style={{fontSize: 20, color: 'white',backgroundColor:'#61a40e', padding: 20 }}
                        styleDisabled={{color: 'red'}}
                        onPress={() => this.addAnotherDate()}
                    >
                    Kliknite ovde da izaberete datum završetka radnog naloga
                </Button>
            </View>
        }
        return (
            <View style={styles.container}>
                {/*<NavigationBar*/}
                {/*title={"Datum"}*/}
                {/*height={50}*/}
                {/*titleColor={'#fff'}*/}
                {/*backgroundColor={'#61a40e'}*/}
                {/*leftButtonTitle="< Nazad"*/}
                {/*leftButtonTitleColor={'#fff'}*/}
                {/*rightButtonTitleColor={'#fff'}*/}
                {/*rightButtonTitle="Potvrdi"*/}
                {/*onRightButtonPress={this.goToWorkOrder.bind(this)}*/}
                {/*onLeftButtonPress={this.goBack.bind(this)}/>*/}
                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.menuItem}>

                        <View style={{ padding: 13}}>
                            {backIcon}

                        </View>

                    </TouchableHighlight>
                    <View style={{ }}>
                        <Text style={{fontSize:19, padding:10,color:'#fff'}}>Datum</Text>
                    </View>
                    <TouchableHighlight onPress={this.goToWorkOrder.bind(this)} style={styles.menuItem}>

                        <View style={{ padding: 13}}>
                            {checkIcon}

                        </View>
                    </TouchableHighlight>

                </View>


                <View style={styles.innerContainer}>
                    <View >
                        <Text style={{marginTop: 30, paddingLeft:10 }}> Izaberi datum</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{width: 50, marginTop:15, alignItems:'flex-start'}}>
                            {calendarIcon}
                        </View>
                        <View>
                            <DatePicker
                                style={{width: 310 ,marginTop:20, paddingRight:30}}
                                date={this.state.dateFrom}
                                mode="date"
                                placeholder="Izaberi datum"
                                format="DD-MM-YYYY"
                                minDate="2012-06-01"
                                showIcon={false}
                                maxDate="01-06-2030"
                                confirmBtnText="Potvrdi"
                                cancelBtnText="Odustani"
                                onDateChange={(date) => {this.setState({dateFrom: date, dateTo: date})}}
                            />

                        </View>
                    </View>
                    <Divider style={styles.divider}/>

                    {renderMore}


                </View>

            </View>
        );

    }


}


function mapStateToProps(state) {

    return {
        today: state.appReducer.today
    };
}

function mapDispatchToProps(dispatch) {
    return {

        setStartTimeForWorkingOrder: (pocetak, zavrsetak) => dispatch(setStartTimeForWorkingOrder(pocetak, zavrsetak))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WorkOrderFrom);


const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        // flexDirection: 'row',
        flex: 1
    },
    innerContainer: {
        padding: 12,

        flex: 1
    },
    listView: {
        flex: 1
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20
    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'
    },
    divider: {
        marginBottom: 20,
        marginTop: 10
    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    }

});
