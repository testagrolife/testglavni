/**
 * Created by basskibo on 6.3.17..
 */
/**
 * Created by basskibo on 6.3.17..
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setChoosenMaterials} from '../actions/AppActions';
import {removeSelectedMachine} from '../actions/AppActions';
import {Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, Dimensions, TextInput} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';


class MaterialsQuantity extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.props.materialToOrder),
            quant: this.props.materialToOrder
        }
    }



    goBack() {
        this.props.navigator.pop();
    }


    finishAndGoToSelectedMaterials() {
        let setChoosenMaterials = {};
        let quantity = parseFloat(this.state.quant._lastNativeText);
        if (this.props.material.unit) {
            setChoosenMaterials = {
                id_materijal: this.props.material.id,
                naziv: this.props.material.naziv,
                jedinica_mere: this.props.material.unit,
                utroseno: quantity,
                forUpdate: false
            };
        } else if (this.props.material.jedinica_mere) {
            setChoosenMaterials = {
                id_materijal: this.props.material.id_materijal,
                naziv: this.props.material.naziv,
                jedinica_mere: this.props.material.jedinica_mere,
                utroseno: quantity,
                forUpdate: true
            };
        }


        this.props.setChoosenMaterials(setChoosenMaterials);
        this.props.navigator.push({
            title: 'Izabrani materijali',
            component: 'selectedMaterials',
        });
    }


    render() {

        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height
        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);
        const backIcon = (<Icon name="chevron-left" size={23} color="#fff"/>);

        if (this.state.isLoading) {
            return <View><Spinner textStyle={{color: '#FFF'}}/></View>;
            //ovaj spiner ne radi!!!!!!!!!!!!! Ne vidi se nista

        }

        let unitCheck = [];
        let quantityCheck = [];


        if (this.props.material.unit) {
            unitCheck.push(
                this.props.material.unit
            )
        } else if (this.props.material.jedinica_mere) {
            unitCheck.push(
                this.props.material.jedinica_mere
            )
        }

        if (!this.props.material.utroseno) {
            quantityCheck.push(
                <View style={{marginTop:10, marginBottom:10,borderWidth: 0.5,borderColor: '#0f0f0f'}}>
                    <Text style={{textAlign:'left', fontSize:15, padding:10 }}>Količina</Text>
                    <TextInput style={{backgroundColor:'#e6e6e6', textAlign:'center'}}
                               keyboardType={'numeric'}
                               ref={(el) => { this.state.quant = el; }} placeholder={"Unesite koliiko je utrošeno"}
                               onChangeText={(quant) => this.setState({quant})}
                    />
                </View>
            )
        } else if (this.props.material.utroseno) {
            quantityCheck.push(
                <View style={{marginTop:10, marginBottom:10,borderWidth: 0.5,borderColor: '#0f0f0f'}}>
                    <Text style={{textAlign:'left', fontSize:15, padding:10 }}>Količina</Text>
                    <TextInput style={{backgroundColor:'#e6e6e6', textAlign:'center'}}
                               keyboardType={'numeric'}
                               ref={(el) => { this.state.quant = el; }} placeholder={"Unesite koliiko je utrošeno"}
                               onChangeText={(quant) => this.setState({quant})}
                    > {this.props.material.utroseno} </TextInput>
                </View>
            )
        }






        return (
            <View style={styles.container}>
                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                        <View style={{ padding: 13}}>
                            {backIcon}

                        </View>

                    </TouchableHighlight>
                    <View style={{ }}>
                        <Text style={{fontSize:19, padding:10,color:'#fff'}}>Unesi količinu</Text>


                    </View>
                    <View>
                        <TouchableHighlight onPress={this.finishAndGoToSelectedMaterials.bind(this)}
                                            style={styles.rowElement}>

                            <View style={{ padding: 13}}>
                                {checkIcon}

                            </View>
                        </TouchableHighlight>
                    </View>

                </View>

                <View style={styles.innerContainer}>
                    <View style={{marginTop:10, marginBottom:10,borderWidth: 0.5,borderColor: '#0f0f0f'}}>
                        <Text style={{textAlign:'left', fontSize:15, padding:10}}>Naziv</Text>
                        <TextInput editable={false}
                                   style={{backgroundColor:'#e6e6e6', color:'#000', textAlign:'center'}}> {this.props.material.naziv}</TextInput>
                    </View>
                    <View style={{marginTop:10, marginBottom:10,borderWidth: 0.5,borderColor: '#0f0f0f'}}>
                        <Text style={{textAlign:'left', fontSize:15, padding:10}}>Merna jedinica</Text>
                        <TextInput editable={false}
                                   style={{backgroundColor:'#e6e6e6',color:'#000', textAlign:'center'}}> {unitCheck}</TextInput>
                    </View>
                    {quantityCheck}





                </View>

            </View>
        );

    }
}

function mapStateToProps(state) {

    return {
        materialToOrder: state.appReducer.materialToOrder,
        materialQuantity: state.appReducer.materialQuantity
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setChoosenMaterials: (materialToOrder) => dispatch(setChoosenMaterials(materialToOrder)),
        removeSelectedMachine: (selectedMachine) => dispatch(removeSelectedMachine(selectedMachine))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MaterialsQuantity);


const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flex: 1
    },
    innerContainer: {
        flex: 1,
        padding: 12
    },
    listView: {
        flex: 1
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20
    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    addButton: {
        marginLeft: 10,
        marginRight: 10
    }

});
