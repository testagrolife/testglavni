import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setStatus } from '../actions/AppActions';
import { Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import NavigationBar from 'react-native-navigation-bar';
import {Divider} from 'react-native-material-design';


class SetStatus extends Component {

    goBack() {
        this.props.navigator.jumpBack();
    }

    setStatusGreen() {
        this.props.setStatus(1);
        this.goBack();
    }
    setStatusYellow() {
        this.props.setStatus(2);
        this.goBack();
    }
    setStatusRed() {
        this.props.setStatus(3);
        this.goBack();
    }
    setStatusLockedDialog() {
        Alert.alert(
            'Da li želite da zaključate radni nalog?',
            'Jednom zaključan nalog više neće moći da se odključa.',
            [
                {text: 'Ne', onPress: () => {} },
                {text: 'Da', onPress: () => { this.setStatusLocked()} },
            ],
            { cancelable: true }
        );
    }
    setStatusLocked() {
        this.props.setStatus(4);
        this.goBack();
    }

    render() {
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>);

        return (
            <View style={styles.container}>

                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement23}>

                        <View style={{ padding: 13}}>
                            {backIcon}

                        </View>
                    </TouchableHighlight>
                    <View style={{ }}>
                        <Text style={{fontSize:19, padding:10,color:'#fff'}}>Status radnog naloga</Text>

                    </View>
                    <View>
                        <View style={styles.rowElement}>

                            <View style={{ padding: 13}}>

                            </View>
                        </View>
                    </View>

                </View>
                <View style={styles.innerContainer}>
                    <View style={styles.rowElement}>
                        <TouchableHighlight onPress={this.setStatusGreen.bind(this)}>
                            <View style={styles.row}>
                                <View style={styles.greenCircle} />
                                <Text style={styles.text}>Otvoren</Text>
                            </View>
                        </TouchableHighlight>
                        <Divider style={styles.divider}/>
                    </View>
                    <View style={styles.rowElement}>
                        <TouchableHighlight onPress={this.setStatusYellow.bind(this)}>
                            <View style={styles.row}>
                                <View style={styles.yellowCircle} />
                                <Text style={styles.text}>U toku</Text>
                            </View>
                        </TouchableHighlight>
                        <Divider style={styles.divider}/>
                    </View>

                    <View style={styles.rowElement}>
                        <TouchableHighlight onPress={this.setStatusRed.bind(this)}>
                            <View style={styles.row}>
                                <View style={styles.lockedCircle} />
                                <Text style={styles.text}>Završen</Text>
                            </View>
                        </TouchableHighlight>
                        <Divider style={styles.divider}/>
                    </View>
                    <View style={styles.rowElement}>
                        <TouchableHighlight onPress={this.setStatusLockedDialog.bind(this)}>
                            <View style={styles.row}>
                                <View style={styles.lockedCircle} >
                                    <Icon name="key"
                                          size={18}
                                          color="#fff"/>
                                </View>
                                <Text style={styles.text}>Zaključan</Text>
                            </View>
                        </TouchableHighlight>
                        <Divider style={styles.divider}/>
                    </View>
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        workOrder: state.appReducer.workOrder
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setStatus: (status) => dispatch(setStatus(status)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SetStatus);


const styles = StyleSheet.create({
   container: {
       flex: 1,
       backgroundColor: '#FFF'
   },
    innerContainer: {
        padding: 12,
      flex: 1
    },
    text: {
        fontSize: 16,
        marginTop:8,
        marginLeft:12
    },
    row: {
        flexDirection: 'row' ,
        padding: 5,
        height:60
    },
    rowElement: {
        padding: 5
    },
    rowHeader: {
        marginBottom:10,
        justifyContent: 'space-between',
        flexDirection: 'row' ,
        fontSize: 18
    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    searchBar: {
        paddingLeft: 30,
        fontSize: 20,
        height: 20,
        borderWidth: 3,
        flex: .1,
        borderColor: '#E4E4E4',
    },
    greenCircle: {
        width: 40,
        height: 40,
        borderRadius: 100/2,
        backgroundColor: 'green',
    },
    yellowCircle: {
        width: 40,
        height: 40,
        borderRadius: 100/2,
        backgroundColor: 'orange'
    },
    lockedCircle: {
        width: 40,
        height: 40,
        borderRadius: 100/2,
        backgroundColor: 'red',
        padding: 10
    }

});

