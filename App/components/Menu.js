import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loginRestCall, logout} from '../actions/AppActions';
import {Alert, StyleSheet, Text, View, TextInput, Image, Navigator,
        TouchableHighlight, AsyncStorage} from 'react-native';
import Button from 'react-native-button';
import {Divider} from 'react-native-material-design';
import Icon from 'react-native-vector-icons/FontAwesome';

import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../assets/fonts/selection.json';
const  IconIcoMoon = createIconSetFromIcoMoon(icoMoonConfig);

class Menu extends Component {

     gotoWorkOrders() {
        this.props.closeControlPanel();
        this.props.navigator.push({title: 'Lista radnih naloga',
                                   component: 'workOrders'});
     }

    gotoParcels() {
        this.props.closeControlPanel();
        this.props.navigator.push({title: 'Lista tabli',
                                   component: 'parcels'});
    }

    logout() {
        this.props.logout();
        AsyncStorage.removeItem("token");
        this.props.navigator.resetTo({ title: 'Login',
                                       component: 'login',
                                       sceneConfig: Navigator.SceneConfigs.FloatFromBottom,
                                       gestures: null});

    }


    render() {
        const menuIcon = (<Icon name="bars" size={23} color="#fff"/>)

        return (
            <View style={styles.menu}>
                <TouchableHighlight onPress={this.gotoWorkOrders.bind(this)} >
                    <View style={styles.rowElement}>
                        <IconIcoMoon name={'4'}
                                     font="icomoon"
                                     style={{fontSize: 23, color:'#fff'}}/>

                        <Text style={styles.text}>Radni Nalozi</Text>
                    </View>
                </TouchableHighlight>
                <Divider style={styles.divider}/>

                <TouchableHighlight onPress={this.gotoParcels.bind(this)} >
                    <View style={styles.rowElement}>
                        <IconIcoMoon name={'5'}
                                     font="icomoon"
                                     style={{fontSize: 23, color:'#fff'}}/>
                        <Text style={styles.text}>Table</Text>
                    </View>
                </TouchableHighlight>
                <Divider style={styles.divider}/>

                {/*<View style={styles.rowElement}>
                    <IconIcoMoon name={'6'}
                                 font="icomoon"
                                 style={{fontSize: 23, color:'#fff'}}/>
                    <Text style={styles.text}>Mape</Text>
                </View>
                <Divider style={styles.divider}/>*/}


                 <TouchableHighlight onPress={this.logout.bind(this)} >
                    <View style={styles.rowElement}>
                        <Icon name={'sign-out'}
                              font="icomoon"
                              style={{fontSize: 23, color:'#fff'}}/>
                        <Text style={styles.text}>Odjavi se</Text>
                    </View>
                </TouchableHighlight>
                <Divider style={styles.divider}/>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        logInFailed: state.appReducer.logInFailed
    };
}

function mapDispatchToProps(dispatch) {
    return {
        loginRestCall: (username, password) => dispatch(loginRestCall(username, password)),
        logout: () => dispatch(logout())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Menu);


const styles = StyleSheet.create({
    menu: {
        flex: 1,
        backgroundColor: '#61a40e',
    },
    rowElement: {
        flexDirection: 'row',
        padding: 13,
    },
    text: {
        fontSize: 18,
        color: '#FFF',
        marginLeft: 12
    },
    divider: {
        height: 3,
        borderColor: '#FFF',
        backgroundColor: '#FFF'
    }
});
