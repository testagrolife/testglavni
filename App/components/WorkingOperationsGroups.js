import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getWorkingOperationsGroups } from '../actions/AppActions';
import { setWorkingOperationsGroupId } from '../actions/AppActions';
import { Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, ScrollView } from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Icon from 'react-native-vector-icons/FontAwesome';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../assets/fonts/selection.json';
const IconIcoMoon = createIconSetFromIcoMoon(icoMoonConfig);

let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class WorkingOperationsGroups extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dataSource: ds.cloneWithRows(this.props.workingOperationsGroups),
        };
    }

    componentWillMount() {
        this.props.getWorkingOperationsGroups();
    }

    componentWillReceiveProps(nextProps){
        this.setState ({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.workingOperationsGroups),
        },function () {
            console.log('setovan  novi state');
        });
    }

    goBack() {
        this.props.navigator.pop();
    }


    renderRow(rowData, sectionID, rowID) {
        let defaultIcon = "20";
        let defaultSize = 20;
        if(rowData.ime_ikonice){
            defaultIcon = rowData.ime_ikonice;
        }
        let colors = ['#00e6e6','#261a0d','#123456', '#fdecba', '#abcdef', '#654321'];
        let style = [{
            height: 40,
            width: 40,
            borderRadius: 20,
            paddingTop: 9,
            textAlign: 'center',
            backgroundColor: colors[rowID % colors.length]
        }];

        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
                   <View style={styles.row}>
                       <Text style={style}>
                          <IconIcoMoon name={defaultIcon}  size={defaultSize}  color={'#fff'}>
                          </IconIcoMoon>
                       </Text>
                       <Text style={styles.text}>
                           {rowData.naziv}
                       </Text>
                   </View>
               </TouchableHighlight>
    }

    onPressRow(rowData){
        console.log('onPressRow: rowData.id: ' + rowData.id);
        //this.props.setWorkingOperationsGroupId(rowData.id);
        this.props.navigator.push({title: 'Izaberi radnu operaciju', component: 'workingOperations', passProps: {id: rowData.id}});
    }

    render() {
        const backIcon = (<Icon style={styles.icon} name="arrow-left"/>);
        const checkIcon = (<Icon style={styles.icon} name="check"/>);
        let content;
        if (this.props.workingOperationsGroups.length > 0) {
            content =
                <View style={styles.mainContainer}>

                    <View
                        style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                        <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                            <View style={{ padding: 13}}>
                                {backIcon}

                            </View>
                        </TouchableHighlight>
                        <View style={{ }}>
                            <Text style={{fontSize:19, padding:10,color:'#fff'}}>Grupe radne operacije</Text>

                        </View>
                        <View>
                            <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                                <View style={{ padding: 13}}>

                                </View>
                            </TouchableHighlight>
                        </View>

                    </View>
                    <View  style={styles.innerContainer}>

                            <ListView
                                dataSource={this.state.dataSource}
                                enableEmptySections={true}
                                renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData,sectionID, rowID)}
                                                       renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator}/>}
                            />


                    </View>
                </View>


        } else {
            content =   <View style={styles.container}>


                <View  style={styles.innerContainer}>
                         <Text>Grupe radnih operacija nisu učitane!</Text>
            </View>
              </View>
        }
        return (
            content
        );
    }
}

function mapStateToProps(state) {
    return {
        loggedIn: state.appReducer.loggedIn,
        workingOperationsGroups: state.appReducer.workingOperationsGroups,
        workingOperationsGroupsFetching: state.appReducer.workingOperationsGroupsFetching,
        workingOperationsGroupsFetchingFailed: state.appReducer.workingOperationsGroupsFetchingFailed
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setWorkingOperationsGroupId: (id) => dispatch(setWorkingOperationsGroupId(id)),
        getWorkingOperationsGroups: () => dispatch(getWorkingOperationsGroups())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WorkingOperationsGroups);


const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFF',
        paddingBottom: 50
    },
    icon: {
        color: '#fff',
        fontSize: 25
    },
    container: {
        flexDirection: 'column',
        flex: 1,
        padding: 12
    },
    innerContainer: {
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20,
        paddingTop: 9
    },
    row: {
        flex: 1,
        flexDirection: 'row' ,
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row' ,
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    }

});