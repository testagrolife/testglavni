import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class example extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Upozorenje
                </Text>
                <Text style={styles.instructions}>
                    Da bi aplikacija radila neophodan je pristup internetu.
                </Text>
                <Text style={styles.instructions}>
                    Proverite vašu internet konekciju.
                </Text>
            </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    }
});

