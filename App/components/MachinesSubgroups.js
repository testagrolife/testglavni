/**
 * Created by ernesthemingvej94 on 3/5/17.
 *//**
 * Created by ernesthemingvej94 on 3/5/17.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getMachinesSubgroups } from '../actions/AppActions';
import { Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, ScrollView } from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Icon from 'react-native-vector-icons/FontAwesome';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../assets/fonts/selection.json';
const IconIcoMoon = createIconSetFromIcoMoon(icoMoonConfig);


let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class MachinesSubgroups extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dataSource: ds.cloneWithRows(this.props.machinesSubgroups),
        };
    }

    goBack() {
        this.props.navigator.pop();
    }

    componentWillMount() {
        this.props.getMachinesSubgroups(this.props.id);
    }


    componentWillReceiveProps(nextProps){
        this.setState ({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.machinesSubgroups)
        },function () {

        });
    }

    renderRow(rowData, sectionID, rowID) {
        let defaultIcon = "9";
        let defaultSize = 20;
        if (this.props.nazivGrupe == "Kombajn"){
           defaultIcon = "10";
           defaultSize = 30;
        }
        if (rowData.ime_ikonice){
            defaultIcon = rowData.ime_ikonice;
            defaultSize = 20;
        }
        let colors = ['#00e6e6','#261a0d','#123456', '#654321', '#fdecba', '#abcdef'];
        let style = [{
            height: 40,
            width: 40,
            borderRadius: 20,
            paddingTop: 9,
            textAlign: 'center',
            backgroundColor: colors[rowID % colors.length]
        }];
        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
                   <View style={styles.row}>
                       <Text style={style}>
                           <IconIcoMoon name={defaultIcon}
                                        size={defaultSize}
                                        color={'#fff'}>
                           </IconIcoMoon>
                       </Text>
                       <Text style={styles.text}>
                           {rowData.naziv}
                       </Text>
                       <Text style={styles.text}>
                           {rowData.ime_ikonice}
                       </Text>
                    </View>
                </TouchableHighlight>
    }

    onPressRow(rowData){
        console.log('onPressRow: rowData.id: ' + rowData.id);
        this.props.navigator.push({title: 'Izaberi pogonsku masinu', component: 'machines', passProps: {idGrupe:this.props.id, idPodgrupe: rowData.id}});
    }




    render() {
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>)
        let content;
        if (this.props.machinesSubgroups.length > 0) {
            content =
                <View style={styles.container}>

                    <View
                        style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                        <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                            <View style={{ padding: 13}}>
                                {backIcon}

                            </View>
                        </TouchableHighlight>
                        <View style={{ }}>
                            <Text style={{fontSize:19, padding:10,color:'#fff'}}>Podgrupa pogonskih mašina</Text>

                        </View>
                        <View>
                            <View style={styles.rowElement}>

                                <View style={{ padding: 13}}>

                                </View>
                            </View>
                        </View>

                    </View>
                    <View  style={styles.innerContainer}>

                        <ListView
                            dataSource={this.state.dataSource}
                            enableEmptySections={true}
                            renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData,sectionID, rowID)}
                                               renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator}/>}
                        />


                    </View>
                </View>


        } else {
            content =   <View style={styles.container}>
                <NavigationBar title={"Podgrupa pogonskih mašina"}
                               height={50}
                               titleColor={'#fff'}
                               backgroundColor={'#61a40e'}
                               leftButtonTitle="< Nazad"
                               leftButtonTitleColor={'#fff'}
                               onLeftButtonPress={this.goBack.bind(this)}/>
                <Text>Podgrupe pogonskih mašina nisu učitane!</Text>
            </View>
        }
        return (
            content
        );
    }
}

function mapStateToProps(state) {
    return {
        loggedIn: state.appReducer.loggedIn,
        machinesSubgroups: state.appReducer.machinesSubgroups,
        machinesSubgroupsFetching: state.appReducer.machinesSubgroupsFetching,
        machinesSubgroupsFetchingFailed: state.appReducer.machinesSubgroupsFetchingFailed
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getMachinesSubgroups: (id) => dispatch(getMachinesSubgroups(id))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MachinesSubgroups);


const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        flex: 1,
    },
    innerContainer: {
        padding: 12
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20,
        paddingTop: 9
    },
    row: {
        flex: 1,
        flexDirection: 'row' ,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row' ,
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    }

});

