import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getWorkingOperationsWithGroupId, setWorkingOperationForWorkOrder} from '../actions/AppActions';
import {getCurrentRouteParams} from '../actions/AppActions';
import {Alert, StyleSheet, Text, TextInput, View, ListView, Image, TouchableHighlight, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';


class WorkingOperations extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            isLoading: true,
            dataSource: ds.cloneWithRows(this.props.workingOperationsForGroupId),
            search: false
        }
    }

    componentWillMount() {

        if (this.props.id) {
            this.props.getWorkingOperationsWithGroupId(this.props.id);
        }

    }


    componentWillReceiveProps(nextProps){

        this.setState({
            isLoading: false,
            dataSource: this.state.dataSource.cloneWithRows(nextProps.workingOperationsForGroupId),
            search: this.state.search
        },function () {
            console.log('Sacekao podatke pre rendera');
            //bez ovoga ne radi...
        });



    }


    renderRow(rowData, sectionID, rowID) {

        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
            <View style={styles.row}>
                <Text style={styles.text}>
                    {rowData.naziv}
                </Text>
            </View>
        </TouchableHighlight>
    }

    onPressRow(rowData) {
        this.props.setWorkingOperationForWorkOrder(rowData.id, rowData.naziv);
        this.props.navigator.push({
            title: 'Novi radni nalog',
            component: 'workOrderSelectedTables',
            passProps: {id_radna_operacija: rowData.id, naziv_radne_operacije: rowData.id}
        });
    }

    goBack() {
        this.props.navigator.pop();
    }

    toggleBar() {
        this.setState({
            search: !this.state.search
        });

    }

    applyFilter(event) {
            let searchText = event.nativeEvent.text;
            let filteredData = [];
            this.props.workingOperationsForGroupId.forEach(function(workingOperation){
               if (workingOperation.naziv.toLowerCase().includes(searchText.toLowerCase())) {
                    filteredData.push(workingOperation)
               }
            });

            this.setState({dataSource: this.state.dataSource.cloneWithRows(filteredData)}, function () {
                console.log('WorkingOperations: setovan  novi state');
            })
        }

        filterNotes(searchText, notes) {

            let text = searchText.toLowerCase();

            return filter(notes, (n) => {
                let note = n.body.toLowerCase();
                return note.search(text) !== -1;
          });
        }



    render() {

        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height
        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);
        const searchIcon = (<Icon name="search" size={23} color="#fff"/>);
        const closeIcon = (<Icon name="close" size={23} color="#fff"/>);
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>);
        if (this.state.isLoading) {
            return <View><Spinner  textStyle={{color: '#FFF'}} /></View>;
            //ovaj spiner ne radi!!!!!!!!!!!!! Ne vidi se nista

        }


        return (
            <View style={styles.container}>

                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                        <View style={{ padding: 13}}>
                            {backIcon}

                        </View>
                    </TouchableHighlight>
                    <View style={{ }}>
                        { this.state.search ? <TextInput
                                style={styles.searchBar}
                                value={this.state.searchText}
                                onChange={this.applyFilter.bind(this)}
                                placeholder='Traži...'/> :
                            <Text style={{fontSize:19, padding:10,color:'#fff'}}>Radne Operacije</Text> }


                    </View>
                    <View>
                        <TouchableHighlight onPress={this.toggleBar.bind(this)} style={styles.rowElement}>

                            <View style={{ padding: 13}}>

                                {this.state.search ? closeIcon : searchIcon}
                            </View>
                        </TouchableHighlight>
                    </View>

                </View>
                <View style={styles.innerContainer}>


                        <ListView
                            style={styles.listView}
                            enableEmptySections={true}
                            dataSource={this.state.dataSource}
                            renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData,sectionID, rowID)}
                            renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator}/>}
                        />


                </View>

            </View>
        );

    }
}

function mapStateToProps(state) {

    return {
        loggedIn: state.appReducer.loggedIn,
        workingOperationsForGroupId: state.appReducer.workingOperationsForGroupId,
        workingOperationsForGroupIdFetching: state.appReducer.workingOperationsForGroupIdFetching,
        workingOperationsForGroupIdFetchingFailed: state.appReducer.workingOperationsForGroupIdFetchingFailed,
        workingOperationsGroupId: state.appReducer.workingOperationsGroupId,
        search: false
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getWorkingOperationsWithGroupId: (id) => dispatch(getWorkingOperationsWithGroupId(id)),
        getCurrentRouteParams: (currentRoutes, currentRoute) => dispatch(getCurrentRouteParams(currentRoutes, currentRoute)),
        setWorkingOperationForWorkOrder: (id, naziv) => dispatch(setWorkingOperationForWorkOrder(id, naziv))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WorkingOperations);



const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#fff',
        color: '#000'
    },
    container: {
        // alignSelf: "stretch",
        // flexDirection: 'row',
        flex: 1,
        // padding: 12
    },
    innerContainer: {
        // marginTop: 40,
        flex: 1,
        backgroundColor: '#fff'
    },
    listView: {
        flex:1
    },
    text: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row' ,
        padding: 5,
        fontSize: 15

    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    searchBar: {
        margin: 3,
        paddingLeft: 10,
        paddingTop: 10,
        fontSize: 20,
        color: "#fff",
        width: 250,
        borderWidth: 1,
        flex: .1,
        borderColor: '#E4E4E4',
    }

});
