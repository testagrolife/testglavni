import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setWorkingOperationForWorkOrder} from '../actions/AppActions';
import {removeSelectedTable} from '../actions/AppActions';
import {setTablesForWorkOrder, setInitialState} from '../actions/AppActions';
import {getCurrentRouteParams, setSelectedTablesFromWorkOrder} from '../actions/AppActions';
import {Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';


class WorkOrderSelectedTables extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.props.workOrderSelectedTables)
        }
    }

    componentWillMount(){
        if(this.props.loadAlreadyChosen === true){
          this.props.setSelectedTablesFromWorkOrder();
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.workOrderSelectedTables)
        }, function () {
            console.log('Sacekao podatke pre rendera');
        });
    }


    renderRow(rowData, rowID) {
        return <View >
            <View style={{flex: 1, flexDirection: 'row', marginBottom:20}}>
                <View style={{width: 300,paddingTop: 15,  alignItems:'flex-start'}}>
                    <Text>Naziv table:{rowData.naziv} </Text>
                </View>

                <TouchableHighlight onPress={this.onPressRow.bind(this , rowID)}
                                    style={{width: 100, paddingTop: 10, marginTop:10, alignItems:'flex-end', paddingRight:80, paddingTop:5}}>
                    <Icon name="remove" size={26} color="gray"/>
                </TouchableHighlight>
            </View>

        </View>
    }

    onPressRow(rowData) {
        console.log("ROWID from table: " + rowData);
        this.props.removeSelectedTable(rowData);

    }

    goBack() {
        this.props.setInitialState('workOrderSelectedTables');
        this.props.navigator.pop();
    }

    goToTables() {
        this.props.navigator.push({
            title: 'Novi radni nalog',
            component: 'workOrderTables',
        });
    }

    finishAndGoToWorkOrder() {

        if(this.props.workOrderSelectedTables.length > 0){
            this.props.setTablesForWorkOrder();
            this.props.setInitialState('workOrderSelectedTables');

            this.props.navigator.push({
                title: 'Novi radni nalog',
                component: 'workOrder',
            });
        }else{
            Alert.alert('Morate odabrati bar jednu tablu!');
        }
    }


    render() {
        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height
        const checkIcon = (<Icon name="check" size={23} color="#fff"/>)
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>)

        if (this.state.isLoading) {
            return <View><Spinner textStyle={{color: '#FFF'}}/></View>;
        }

        return (
            <View style={styles.container}>

                <View
                    style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>

                        <View style={{ padding: 13}}>
                            {backIcon}

                        </View>

                    </TouchableHighlight>
                    <View style={{ }}>
                        <Text style={{fontSize:19, padding:10,color:'#fff'}}>Lista Tabli</Text>


                    </View>
                    <View>
                        <TouchableHighlight onPress={this.finishAndGoToWorkOrder.bind(this)} style={styles.rowElement}>

                            <View style={{ padding: 13}}>
                                {checkIcon}

                            </View>
                        </TouchableHighlight>
                    </View>

                </View>
                <View style={styles.innerContainer}>

                    <ListView
                        style={styles.listView}
                        enableEmptySections={true}
                        dataSource={this.state.dataSource}
                        renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData,sectionID, rowID)}
                        renderSeparator={(sectionId, rowId) =>
                         <View key={rowId} style={styles.separator}/>}
                    />


                    <View style={styles.addButton}>

                        <Icon.Button name="plus-circle"
                                     backgroundColor="#61a40e"
                                     onPress={ this.goToTables.bind(this) }
                                     size={30}
                                     padding={20}
                                     marginLeft={10}
                                     marginRight={10}
                                     borderRadius={7}
                                     style={styles.addButton}
                                     iconStyle={{marginRight: 20, marginRight:20 }}
                                     fontSize={8}>
                            <Text style={{ fontSize: 20, color: 'white'}}>
                                Dodaj tablu
                            </Text>
                        </Icon.Button>
                    </View>


                </View>

            </View>
        );

    }
}

function mapStateToProps(state) {
    return {
        workOrderSelectedTables: state.appReducer.workOrderSelectedTables
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setInitialState: (component) => dispatch(setInitialState(component)),
        setWorkingOperationForWorkOrder: (id, naziv) => dispatch(setWorkingOperationForWorkOrder(id, naziv)),
        removeSelectedTable: (table) => dispatch(removeSelectedTable(table)),
        setTablesForWorkOrder: () => dispatch(setTablesForWorkOrder()),
        setSelectedTablesFromWorkOrder: () => dispatch(setSelectedTablesFromWorkOrder())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WorkOrderSelectedTables);


const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flex: 1,
    },
    innerContainer: {
        margin: 10,
        flex: 1
    },
    listView: {
        flex: 1
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20
    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    addButton: {
        marginLeft: 10,
        marginRight: 10
    }

});
