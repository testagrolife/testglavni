import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getWorkOrders } from '../actions/AppActions';
import { getWorkOrderById } from '../actions/AppActions';
import { Alert, StyleSheet, Text, View, ListView, Image,
         TouchableHighlight, BackAndroid} from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/FontAwesome';
import Moment from 'moment';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';

let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class WorkOrders extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dataSource: ds.cloneWithRows(this.props.workOrders),
            showNoWorkOrderMessage: false
        };
    }

    componentWillMount() {
        this.props.getWorkOrders();
    }

    componentWillReceiveProps(nextProps) {
    if(nextProps.workOrders.length){
        this.setState({dataSource: this.state.dataSource.cloneWithRows(nextProps.workOrders), showNoWorkOrderMessage: false}, function () {
            console.log('setovan  novi state');
        })

    }else{
       this.setState({showNoWorkOrderMessage: true}, function () {
                  console.log('setovan  novi state');
          })
     }
    }

    renderRow(rowData, sectionID, rowID) {
         // let date = Moment(rowData.datum_pocetka).format('YYYY-MM-DD');
         // if (date === 'Invalid date'){
         //    return null;
         // }
         // Da se ne bi videli stari radni nalozi koji su shit!! Obrisati kad se predje na PROD bazu
        let row = rowData.datum_pocetka + ', ' + rowData.naziv_radne_operacije;
        let semaphore = this.renderSemaphore(rowData);
        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
                   <View style={styles.row}>
                       {semaphore}
                       <Text style={styles.text}>
                           {row}
                       </Text>
                   </View>
            </TouchableHighlight>

    }

    renderSemaphore(workOrder) {
        let circle;
        let status = workOrder.id_status ? workOrder.id_status : 1;
        switch (status) {
            case 1:
                circle = <View style={styles.greenCircle} />
                break;
            case 2:
                circle = <View style={styles.yellowCircle} />
                break;
            case 3:
                circle = <View style={styles.lockedCircle} />
                break;
            case 4:
                circle = <View style={styles.lockedCircle} >
                            <Icon name="key"
                                  size={9}
                                  color="#fff"/>
                         </View>
                break;

            default:
                circle = <View style={styles.greenCircle} />

        }
        return circle
    }

    createNewWorkOrder() {
        this.props.navigator.push({title: 'Grupa radnih operacija', component: 'workingOperationsGroups'});
    }


    onPressRow(rowData) {
        this.props.getWorkOrderById(rowData.id);
        this.props.navigator.push({title: 'Novi radni nalog', component: 'workOrder'});

    }

    goBack() {
        this.props.navigator.push({title:'Home screen', component:'home'});
    }


    render() {
        let contentForNoWorkOrder;
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>);

             if(this.state.showNoWorkOrderMessage == true){
               contentForNoWorkOrder = <Text style={{marginTop:80, backgroundColor: '#61a40e', color: '#fff', fontSize:16, padding:15, textAlign: 'center'}}>Još uvek niste kreirali ni jedan radni nalog!</Text>
             }
             return (
                <View style={styles.container}>
                    <View
                        style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                        <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement23}>

                            <View style={{ padding: 13}}>
                                {backIcon}

                            </View>
                        </TouchableHighlight>
                        <View style={{ }}>
                            <Text style={{fontSize: 19, padding: 10, color: '#fff'}}>Lista radnih naloga</Text>

                        </View>
                        <View>
                            <View style={styles.rowElement}>

                                <View style={{ padding: 13}}>

                                </View>
                            </View>
                        </View>

                    </View>

                <View  style={styles.innerContainer}>

                    {contentForNoWorkOrder}

                    <Spinner
                        visible={this.state.workOrdersFetching}
                        textContent={"Učitavanje..."}
                        textStyle={{color: '#61a40e'}} />


                    <ListView
                        dataSource={this.state.dataSource}
                        enableEmptySections={true}
                        renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData,sectionID, rowID)}
                        renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator}/>}
                    />






                </View>
                    <ActionButton buttonColor="rgb(56, 28, 216)"
                                  position='right'
                                  verticalOrientation='up'
                                  onPress={() => { this.createNewWorkOrder()}}
                    />
                </View>







        );
    }
}

function mapStateToProps(state) {
    return {
        loggedIn: state.appReducer.loggedIn,
        workOrders: state.appReducer.workOrders,
        workOrdersFetching: state.appReducer.workOrdersFetching
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getWorkOrders: () => dispatch(getWorkOrders()),
        getWorkOrderById: (id) => dispatch(getWorkOrderById(id))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WorkOrders);


const styles = StyleSheet.create({
   container: {
       flex: 1,
       backgroundColor: '#FFF',
   },
    innerContainer: {
        padding: 12,
        marginBottom: 50
    },

    text: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row' ,
        padding: 5,
        fontSize: 15

    },
    row: {
        flex: 1,
        flexDirection: 'row' ,
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row' ,
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    greenCircle: {
        marginTop: 5,
        marginRight: 5,
        width: 20,
        height: 20,
        borderRadius: 100/2,
        backgroundColor: 'green',
    },
    yellowCircle: {
        marginTop: 5,
        marginRight: 5,
        width: 20,
        height: 20,
        borderRadius: 100/2,
        backgroundColor: 'orange'
    },
    lockedCircle: {
        marginTop: 5,
        marginRight: 5,
        width: 20,
        height: 20,
        borderRadius: 100/2,
        backgroundColor: 'red',
        padding: 5
    }

});

