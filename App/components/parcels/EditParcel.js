import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setCultures, getParcels} from '../../actions/ParcelsActions';
import {StyleSheet, Text, View, TouchableHighlight} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import {Divider} from 'react-native-material-design';
import Icon from 'react-native-vector-icons/FontAwesome';


class EditParcel extends Component {
    goBack() {
        this.props.navigator.pop();
    }

    save() {
        this.props.setCultures(this.props.rowData.id, this.props.cultureId);
        this.props.getParcels();
        this.props.navigator.pop();
    }

    goToChooseCulture() {
        this.props.navigator.push({title: 'Choose Group',
                                   component: 'cultureGroups',
                                   passProps: {idParcel: this.props.rowData.id}});
    }

    render() {
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>);
        const checkIcon =(<Icon name="check" size={23} color="#fff"/>)

        let title = "...";
        if (this.props.rowData.id_kultura !== undefined) {
             title = this.props.rowData.id_kultura.naziv;
        }

        let culture;

        if (this.props.cultureName) {
            culture = this.props.cultureName;
        } else if (this.props.rowData.id_kultura !== undefined) {
            culture = this.props.rowData.id_kultura.naziv;
        } else  {
            culture = "...";
        }

        let navBar = null;
        if (!!this.props.cultureId) {
            navBar = <View style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                          <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>
                              <View style={{ padding: 13}}>
                                  {backIcon}
                              </View>
                          </TouchableHighlight>
                          <View style={{ }}>
                              <Text style={{fontSize:19, padding:10,color:'#fff'}}>{culture}</Text>
                          </View>
                          <TouchableHighlight onPress={this.save.bind(this)} style={styles.menuItem}>
                              <View style={{ padding: 13}}>
                                  {checkIcon}
                              </View>
                          </TouchableHighlight>

                     </View>;
        } else {
            navBar = <View style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                         <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>
                             <View style={{ padding: 13}}>
                                 {backIcon}
                             </View>
                         </TouchableHighlight>
                         <View style={{ }}>
                             <Text style={{fontSize:19, padding:10,color:'#fff'}}>{culture}</Text>
                         </View>
                         <View>
                            <View style={styles.rowElement}>
                                <View style={{ padding: 13}}>
                                </View>
                           </View>
                       </View>
                     </View>
        }

        return (
            <View style={styles.container}>

                {navBar}

                <View style={styles.innerContainer}>
                    <View >
                        <Text style={styles.text}>Broj:  {this.props.rowData.naziv}</Text>
                        <View style={styles.separator}/>
                        <Text style={styles.text}>Površina:  {this.props.rowData.povrsina}</Text>
                        <View style={styles.separator}/>
                        <Text style={styles.text}>Kultura:  {culture}</Text>
                    </View>


                    <Icon.Button name="plus-circle"
                                 backgroundColor="#61a40e"
                                 onPress={ this.goToChooseCulture.bind(this) }
                                 size={30}
                                 padding={20}
                                 marginLeft={10}
                                 marginRight={10}
                                 borderRadius={7}
                                 style={styles.addButton}
                                 iconStyle={{marginRight: 20, marginRight:20 }}
                                 fontSize={8}>
                        <Text style={{ fontSize: 20, color: 'white'}}>
                            Odaberi kulturu
                        </Text>
                    </Icon.Button>
                </View>
            </View>
        );

    }
}

function mapStateToProps(state) {

    return {
        cultures: state.parcelsReducer.cultures,
        culturesFetching: state.parcelsReducer.culturesFetching,
        culturesFetchingFailed: state.parcelsReducer.culturesFetchingFailed,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getCultures: (id) => dispatch(getCultures(id)),
        getParcels: () => dispatch(getParcels()),
        setCultures: (idParcel, idCulture) => dispatch(setCultures(idParcel, idCulture)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditParcel);

const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flex: 1,
        backgroundColor: '#FFF'
    },
    innerContainer: {
        padding: 12,
        flex:1
    },
    listView: {
        flex: 1
    },
    text: {
        marginLeft: 12,
        fontSize: 18,
        padding: 20,
    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15,
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center',
    },
    divider: {
        marginBottom: 20,
        marginTop: 10,
    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    }
});
