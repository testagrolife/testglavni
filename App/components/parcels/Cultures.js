import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getCultures, setCultures} from '../../actions/ParcelsActions';
import {Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, Navigator, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';

class Cultures extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.props.cultures)
        }
    }

    componentWillMount(){
        this.props.getCultures(this.props.rowData.id);
    }


    componentWillReceiveProps(nextProps) {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.cultures)
        }, function () {
            console.log('Cultures: Sacekao podatke pre rendera');
        });
    }


    renderRow(rowData, sectionID, rowID) {
        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
            <View style={{flex: 1, flexDirection: 'row', marginBottom:20}}>
                <View style={{width: 300,paddingTop: 15,  alignItems:'flex-start'}}>
                    <Text>{rowData.naziv} </Text>
                </View>
            </View>
        </TouchableHighlight>

    }

    onPressRow(rowData) {
        let routes = this.props.navigator.state.routeStack;
        for (let i = routes.length - 1; i >= 0; i--) {
            if(routes[i].component === "editParcel"){
                let destinationRoute = this.props.navigator.getCurrentRoutes()[i];
                destinationRoute.passProps.cultureId = rowData.id;
                destinationRoute.passProps.cultureName = rowData.naziv;
                this.props.navigator.popToRoute(destinationRoute);
            }
        }
    }

    goBack() {
        this.props.navigator.pop();
    }

    render() {
        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>);
        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);

        let content;
        if (this.props.culturesFetching) {
            content = <View><Spinner textStyle={{color: '#FCF'}}/></View>;
        }


        return (
            <View style={styles.container}>

                <View style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>
                        <View style={{ padding: 13}}>
                            {backIcon}
                        </View>
                    </TouchableHighlight>
                    <View style={{ }}>
                        <Text style={{fontSize:19, padding:10,color:'#fff'}}>Kulture</Text>
                    </View>
                    <View>
                        <View style={styles.rowElement}>
                            <View style={{ padding: 13}}>
                            </View>
                        </View>
                    </View>
                </View>

                <View style={styles.innerContainer}>

                    <ListView
                        style={styles.listView}
                        enableEmptySections={true}
                        dataSource={this.state.dataSource}
                        renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData,sectionID, rowID)}
                        renderSeparator={(sectionId, rowId) =>
                            <View key={rowId} style={styles.separator}/>}
                    />
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {

    return {
        cultures: state.parcelsReducer.cultures,
        culturesFetching: state.parcelsReducer.culturesFetching,
        culturesFetchingFailed: state.parcelsReducer.culturesFetchingFailed,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getCultures: (id) => dispatch(getCultures(id)),
        setCultures: (idParcel, idCulture) => dispatch(setCultures(idParcel, idCulture)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Cultures);


const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flex: 1,
        backgroundColor: '#FFF'
    },
    innerContainer: {
        padding: 12,
        flex:1
    },
    listView: {
        flex: 1
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20
    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    addButton: {
        marginLeft: 10,
        marginRight: 10
    }

});
