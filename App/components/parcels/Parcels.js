import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getParcels} from '../../actions/ParcelsActions';
import {Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, TextInput} from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/FontAwesome';
import NavigationBar from 'react-native-navigation-bar';


class Parcels extends Component {

    constructor(props) {
        super(props);

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.props.parcels),
        };
    }

    componentDidMount() {
        console.log('Parcels: componentDidMount');
        this.props.getParcels();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({dataSource: this.state.dataSource.cloneWithRows(nextProps.parcels)}, function () {
            console.log('Parcels: setovan  novi state');
        })
    }

    renderRow(rowData, sectionID, rowID) {
        let row = rowData.naziv + " " + rowData.povrsina + rowData.kultura;
        let kultura = '';
        if (rowData.id_kultura && rowData.id_kultura.naziv) {
            kultura = rowData.id_kultura.naziv;
        }
        return <TouchableHighlight onPress={this.onPressRow.bind(this, rowData)}>
            <View style={styles.row}>
                <Text style={[styles.row, styles.text]}>{rowData.naziv}</Text>
                <Text style={[styles.row, styles.text]}>{rowData.povrsina}</Text>
                <Text style={[styles.row, styles.text]}>{kultura}</Text>
            </View>
        </TouchableHighlight>

    }

    onPressRow(rowData) {
        this.props.navigator.push({
            title: 'Edit Parcel',
            component: 'editParcel',
            passProps: {'rowData': rowData}
        });
    }

    goBack() {
        this.props.navigator.pop();
    }

    applyFilter(event) {
        let searchText = event.nativeEvent.text;
        let filteredData = [];
        this.props.parcels.forEach(function (parcel) {
            if (parcel.naziv.startsWith(searchText)) {
                filteredData.push(parcel)
            }
        });

        this.setState({dataSource: this.state.dataSource.cloneWithRows(filteredData)}, function () {
            console.log('Parcels: setovan  novi state');
        })
    }

    filterNotes(searchText, notes) {

        let text = searchText.toLowerCase();

        return filter(notes, (n) => {
            let note = n.body.toLowerCase();
            return note.search(text) !== -1;
        });
    }

    toggleBar() {
        this.setState({
            search: !this.state.search
        });

    }


    render() {
        let content;
        const searchIcon = (<Icon name="search" size={23} color="#fff"/>);
        const closeIcon = (<Icon name="close" size={23} color="#fff"/>);
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>);
        if (this.props.parcels.length > 0) {
            content =
                <View style={styles.container}>

                    <View
                        style={{
                            flexDirection: 'row',
                            backgroundColor: '#61a40e',
                            justifyContent: 'space-between',
                            height: 50
                        }}>
                        <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement23}>

                            <View style={{padding: 13}}>
                                {backIcon}

                            </View>
                        </TouchableHighlight>
                        <View style={{}}>
                            { this.state.search ? <TextInput
                                style={styles.searchBar}
                                value={this.state.searchText}
                                onChange={this.applyFilter.bind(this)}
                                placeholder='Traži...'/> :
                                <Text style={{fontSize: 19, padding: 10, color: '#fff'}}>Lista parcela</Text> }
                        </View>
                        <View>
                            <View style={styles.rowElement}>
                                <TouchableHighlight onPress={this.toggleBar.bind(this)} style={styles.rowElement}>

                                    <View style={{padding: 13}}>
                                        {this.state.search ? closeIcon : searchIcon}

                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>

                    </View>
                    <View style={styles.innerContainer}>


                        <View style={styles.rowHeader}>
                            <Text style={styles.row}>Broj</Text>
                            <Text style={styles.row}>Površina</Text>
                            <Text style={styles.row}>Kultura</Text>
                        </View>

                        <ListView
                            dataSource={this.state.dataSource}
                            enableEmptySections={true}
                            renderRow={(rowData, sectionID, rowID) => this.renderRow(rowData, sectionID, rowID)}
                            renderSeparator={(sectionId, rowId) =>
                                <View key={rowId} style={styles.separator}/>}
                        />

                    </View>
                </View>


        } else {
            content = <View style={styles.container}>
                <View
                    style={{
                        flexDirection: 'row',
                        backgroundColor: '#61a40e',
                        justifyContent: 'space-between',
                        height: 50
                    }}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement23}>

                        <View style={{padding: 13}}>
                            {backIcon}

                        </View>
                    </TouchableHighlight>
                    <View style={{}}>
                        { this.state.search ? <TextInput
                            style={styles.searchBar}
                            value={this.state.searchText}
                            onChange={this.applyFilter.bind(this)}
                            placeholder='Traži...'/> :
                            <Text style={{fontSize: 19, padding: 10, color: '#fff'}}>Lista parcela</Text> }
                    </View>
                    <View>
                        <View style={styles.rowElement}>
                            <TouchableHighlight onPress={this.toggleBar.bind(this)} style={styles.rowElement}>

                                <View style={{padding: 13}}>
                                    {this.state.search ? closeIcon : searchIcon}

                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>

                </View>
                <Text style={{marginTop: 40}}>učitavam podatke...</Text>
            </View>
        }
        return (
            content
        );
    }
}

function mapStateToProps(state) {
    return {
        parcels: state.parcelsReducer.parcels,
        parcelsFetching: state.parcelsReducer.parcelsFetching,
        parcelsFetchingFailed: state.parcelsReducer.parcelsFetchingFailed,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getParcels: () => dispatch(getParcels()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Parcels);


const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        flex: 1,
        backgroundColor: '#FFF'
    },
    innerContainer: {
        padding: 12,
        flex: 1
    },
    text: {
        fontSize: 18,
    },
    row: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        padding: 5,
    },
    rowHeader: {
        marginBottom: 10,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    searchBar: {
        margin: 3,
        paddingLeft: 10,
        paddingTop: 10,
        fontSize: 20,
        color: "#fff",
        width: 250,
        borderWidth: 1,
        flex: .1,
        borderColor: '#E4E4E4',
    }

});

