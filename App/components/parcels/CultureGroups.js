import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getCultureGroups} from '../../actions/ParcelsActions';
import {Alert, StyleSheet, Text, View, ListView, Image, TouchableHighlight, Dimensions} from 'react-native';
import NavigationBar from 'react-native-navigation-bar';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';

import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../../assets/fonts/selection.json';
const IconIcoMoon = createIconSetFromIcoMoon(icoMoonConfig);

class CultureGroups extends Component {

    constructor(props) {
        super(props)

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.props.cultureGroups)
        }
    }

    componentWillMount(){
        this.props.getCultureGroups();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.cultureGroups)
        }, function () {
            console.log('CultureGroups: Sacekao podatke pre rendera');
        });
    }


    renderRow(rowData, sectionID, rowID) {
        let iconName = "20";
        let defaultSize = 20;
        switch (rowData.naziv) {
            case 'Žitarice':
                iconName = 'zitarice';
                break;
            case 'Industrijsko bilje':
                iconName = 'industrisko_bilje';
                break;
            case 'Krmno bilje':
                iconName = 'krmno_bilje';
                break;
            case 'Aromatično i lekovito bilje':
                iconName = 'lekovito_bilje';
                break;
            case 'Povrće':
                iconName = 'povrce';
                break;
            case 'Voće i grožđe':
                iconName = 'voce_i_grozdje';
                break;
            case 'Neobraženo zemljište':
                iconName = '5';
                break;

            default:
                iconName = '5';
        }

        let colors = ['#00e6e6','#261a0d','#123456', '#fdecba', '#abcdef', '#654321'];
        let style = [{
            height: 40,
            width: 40,
            borderRadius: 20,
            paddingTop: 9,
            textAlign: 'center',
            backgroundColor: colors[rowID % colors.length]
        }];


        return <TouchableHighlight onPress={this.onPressRow.bind(this , rowData)}>
                  <View style={styles.row}>
                      <Text style={style}>
                         <IconIcoMoon name={iconName}
                                      size={defaultSize}
                                      color={'#fff'}>
                         </IconIcoMoon>
                      </Text>
                      <Text style={styles.text}>
                          {rowData.naziv}
                      </Text>
                  </View>
              </TouchableHighlight>
    }

    onPressRow(rowData) {
        this.props.navigator.push({title: 'cultures',
                                  component: 'cultures',
                                  passProps: {'rowData': rowData,
                                              'idParcel': this.props.idParcel}});
    }

    goBack() {
        this.props.navigator.pop();
    }

    render() {
        let width = Dimensions.get('window').width; //full width
        let height = Dimensions.get('window').height; //full height
        const backIcon = (<Icon name="arrow-left" size={23} color="#fff"/>);
        const checkIcon = (<Icon name="check" size={23} color="#fff"/>);

        if (this.props.cultureGroupsFetching) {
            return <Spinner textStyle={{color: '#FCF', marginTop: 100}}/>;
        }

        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', backgroundColor:'#61a40e', justifyContent: 'space-between',height:50}}>
                    <TouchableHighlight onPress={this.goBack.bind(this)} style={styles.rowElement}>
                        <View style={{ padding: 13}}>
                            {backIcon}
                        </View>
                    </TouchableHighlight>
                    <View style={{ }}>
                        <Text style={{fontSize:19, padding:10,color:'#fff'}}>Grupa kultura</Text>
                    </View>
                    <View>
                        <View style={styles.rowElement}>
                            <View style={{ padding: 13}}>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.innerContainer}>

                    <ListView
                        style={styles.listView}
                        enableEmptySections={true}
                        dataSource={this.state.dataSource}
                        renderRow={(rowData,sectionID, rowID) => this.renderRow(rowData,sectionID, rowID)}
                        renderSeparator={(sectionId, rowId) =>
                            <View key={rowId} style={styles.separator}/>}
                    />
                </View>
            </View>
        );

    }
}

function mapStateToProps(state) {

    return {
        cultureGroups: state.parcelsReducer.cultureGroups,
        cultureGroupsFetching: state.parcelsReducer.cultureGroupsFetching,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getCultureGroups: () => dispatch(getCultureGroups())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CultureGroups);


const styles = StyleSheet.create({
    container: {
        alignSelf: "stretch",
        flex: 1,
        backgroundColor: '#FFF'
    },
    innerContainer: {
        padding: 12,
        flex:1
    },
    listView: {
        flex: 1
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
        paddingRight: 20,
        paddingTop: 9
    },
    row: {
        flexDirection: 'row',
        marginRight: 10,
        padding: 15
    },
    photo: {
        height: 25,
        width: 25,
        flexDirection: 'row',
        alignItems: 'flex-end',
        //alignSelf: 'flex-end',
        justifyContent: 'center'

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E'
    },
    addButton: {
        marginLeft: 10,
        marginRight: 10
    }

});
