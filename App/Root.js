import React, { Component } from 'react';
import { Navigator, Text, TouchableHighlight, AsyncStorage,
         Menu, BackAndroid, NetInfo } from 'react-native';
import {setTokenInReducer} from './actions/AppActions';
import { connect } from 'react-redux';
import Login from './components/Login';
import WorkOrders from './components/WorkOrders';
import HomeScreen from './components/HomeScreen';
import WorkingOperationsGroups from './components/WorkingOperationsGroups';
import WorkingOperations from './components/WorkingOperations';
import WorkOrder from './components/WorkOrder';
import WorkOrderSelectedTables from './components/WorkOrderSelectedTables';
import WorkOrderFrom from './components/WorkOrderFrom';
import WorkOrderTables from './components/WorkOrderTables';
import SelectedMachines from './components/SelectedMachines';
import MachinesSubgroups from './components/MachinesSubgroups';
import MachinesGroups from './components/MachinesGroups';
import Machines from './components/Machines';
import SelectedMaterials from './components/SelectedMaterials';
import MaterialSubgroups from './components/MaterialSubgroups';
import MaterialGroups from './components/MaterialGroups';
import Materials from './components/Materials';
import MaterialsQuantity from './components/MaterialsQuantity';
import SelectedHelpMachines from './components/SelectedHelpMachines';
import HelpMachinesGroups from './components/HelpMachinesGroups';
import HelpMachines from './components/HelpMachines';
import Parcels     from './components/parcels/Parcels';
import EditParcel from './components/parcels/EditParcel';
import CultureGroups from './components/parcels/CultureGroups';
import Cultures from './components/parcels/Cultures';
import SetStatus from './components/SetStatus';
import Offline from './components/offline/Offline';
import {changeConnectionStatus} from './actions/OfflineActions';

class Root extends Component {

    constructor(props) {
       super(props);
       this.state = {
         token : this.props.token
       }
    }

    componentWillMount(){
        console.log('Root.js: willMount')
        AsyncStorage.getItem("token").then((token) => {
                console.log('Root.js: token: ' + token)
               return token
           }).then((token) => {
               console.log('Root.js: token: ' + token)
               if(token){
                  this.setState({token: token}, function () {
                            console.log('setovan  novi state');
                  })
               this.props.setTokenInReducer(token);
             }
        });

//        BackAndroid.addEventListener("hardwareBackPress", () => {
//            if (this.goBack) {
//                  this.goBack();
//                  return true // do not exit app
//              } else {
//                return false // exit app
//              }
//            })

        NetInfo.isConnected.addEventListener('change', this._handleConnectionChange);
    }

    _handleConnectionChange = (isConnected) => {
        this.props.changeConnectionStatus(isConnected);
    };

    navigatorRenderScene(route, navigator) {
        switch (route.component) {
            case 'login':
                return  <Login navigator={navigator} {...route.passProps}/>;

            case 'home':
                return  <HomeScreen navigator={navigator} {...route.passProps}/>;

            case 'workOrders':
                return  <WorkOrders navigator={navigator} {...route.passProps}/>;
            case 'workOrder':
                return  <WorkOrder navigator={navigator} {...route.passProps}/>;
            case 'workingOperationsGroups':
                return  <WorkingOperationsGroups navigator={navigator} {...route.passProps}/>;
            case 'workOrderFrom':
                return <WorkOrderFrom navigator={navigator} {...route.passProps}/>;

            case 'workingOperations':
                return <WorkingOperations navigator={navigator} {...route.passProps}/>;
            case 'workOrderSelectedTables':
                return <WorkOrderSelectedTables navigator={navigator} {...route.passProps}/>;
            case 'workOrderTables':
                return <WorkOrderTables navigator={navigator} {...route.passProps}/>;

            case 'selectedMachines':
                return <SelectedMachines navigator={navigator} {...route.passProps}/>;
            case 'machinesSubgroups':
                return <MachinesSubgroups navigator={navigator} {...route.passProps}/>;
            case 'machinesGroups':
                return <MachinesGroups navigator={navigator} {...route.passProps}/>;
            case 'machines':
                return <Machines navigator={navigator} {...route.passProps}/>;
            case 'selectedMaterials':
                return <SelectedMaterials navigator={navigator} {...route.passProps}/>;
            case 'materialSubgroups':
                return <MaterialSubgroups navigator={navigator} {...route.passProps}/>;
            case 'materialGroups':
                return <MaterialGroups navigator={navigator} {...route.passProps}/>;
            case 'materials':
                return <Materials navigator={navigator} {...route.passProps}/>;
            case 'materialsQuantity':
                return <MaterialsQuantity navigator={navigator} {...route.passProps}/>;
            case 'selectedHelpMachines':
                return <SelectedHelpMachines navigator={navigator} {...route.passProps}/>;
            case 'helpMachinesGroups':
                return <HelpMachinesGroups navigator={navigator} {...route.passProps}/>;
            case 'helpMachines':
                return <HelpMachines navigator={navigator} {...route.passProps}/>;
            case 'parcels':
                return <Parcels navigator={navigator} {...route.passProps}/>;
            case 'editParcel':
                return <EditParcel navigator={navigator} {...route.passProps}/>;
            case 'cultureGroups':
                return <CultureGroups navigator={navigator} {...route.passProps}/>;
            case 'cultures':
                return <Cultures navigator={navigator} {...route.passProps}/>;
            case 'setStatus':
                return <SetStatus navigator={navigator} {...route.passProps}/>;


            default:
                return <Login/>;
        }
    }

    render() {
        const routes = [
            {title: 'Login', component: 'login'},
            {title: 'Radna tabla', component: 'home'} //,
        ];
        let firstPageToServe = 0;
        if (this.state.token){
            firstPageToServe = 1
        }

        // Offline mode screen
        let content;
        if (!this.props.status) {
             return ( <Offline />);

        }
        return (
            <Navigator initialRoute={routes[firstPageToServe]}
                       initialRouteStack={routes}
                       renderScene={this.navigatorRenderScene} />
        );
    }
}

function mapStateToProps(state) {
    return {
        token: null,
        status: state.offlineReducer.status
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setTokenInReducer: (token) => dispatch(setTokenInReducer(token)),
        changeConnectionStatus: (status) => dispatch(changeConnectionStatus(status))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Root);
