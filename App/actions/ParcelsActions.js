import axios from 'axios';
import { AsyncStorage } from 'react-native';

export const Type = {
    GET_PARCELS_CALL:    'GET_PARCELS_CALL',
    GET_PARCELS_SUCCESS: 'GET_PARCELS_SUCCESS',
    GET_PARCELS_FAILED:  'GET_PARCELS_FAILED',

    GET_CULTURE_GROUPS_CALL:    'GET_CULTURE_GROUPS_CALL',
    GET_CULTURE_GROUPS_SUCCESS: 'GET_CULTURE_GROUPS_SUCCESS',
    GET_CULTURE_GROUPS_FAILED:  'GET_CULTURE_GROUPS_FAILED',

    GET_CULTURES_CALL:    'GET_CULTURES_CALL',
    GET_CULTURES_SUCCESS: 'GET_CULTURES_SUCCESS',
    GET_CULTURES_FAILED:  'GET_CULTURES_FAILED',

    SET_CULTURES_CALL:    'SET_CULTURES_CALL',
    SET_CULTURES_SUCCESS: 'SET_CULTURES_SUCCESS',
    SET_CULTURES_FAILED:  'SET_CULTURES_FAILED',

};

export function getParcels() {
    return (dispatch) => {
        dispatch({
            type: Type.GET_PARCELS_CALL
        });

        AsyncStorage.getItem("token").then((token) => {
            console.log('getParcels token:  ' + token);
            var config = {
                headers: {'Authorization': 'Bearer [' + token + ']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/parcel/getParcelsWithRelation', config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_PARCELS_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_PARCELS_FAILED
                    });
                });
        });
    }
}

export function getCultureGroups() {
    return (dispatch) => {
        dispatch({
            type: Type.GET_CULTURE_GROUPS_CALL
        });

        AsyncStorage.getItem("token").then((token) => {
            console.log('getParcels token:  ' + token);
            var config = {
                headers: {'Authorization': 'Bearer [' + token + ']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/culture/getGroups', config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_CULTURE_GROUPS_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_CULTURE_GROUPS_FAILED
                    });
                });
        });
    }
}

export function getCultures(id) {
    return (dispatch) => {
        dispatch({
            type: Type.GET_CULTURES_CALL
        });

        AsyncStorage.getItem("token").then((token) => {
            console.log('getParcels token:  ' + token);
            var config = {
                headers: {'Authorization': 'Bearer [' + token + ']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/culture/getByGroup?id_kultura_grupa='+id, config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_CULTURES_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_CULTURES_FAILED
                    });
                });
        });
    }
}

export function setCultures(idParcel, idCulture) {
    return (dispatch) => {
        dispatch({
            type: Type.SET_CULTURES_CALL
        });

        AsyncStorage.getItem("token").then((token) => {
            console.log('getParcels token:  ' + token);
            var config = {
                headers: {'Authorization': 'Bearer [' + token + ']'},
            };
            return config;

        }).then((config) => {
            console.log('setCultures - id_kultura' + idCulture + " "+ 'id': idParcel);
            return axios.post('http://agrolife.greensoft.co/parcel/updateCultureForParcel',
                 {
                    'id_kultura': idCulture,
                    'id': idParcel
                 }
                 , config)
                .then(function (response) {
                    console.log("setCultures -SUCCESS, response="+JSON.stringify(response));
                    dispatch({
                        type: Type.SET_CULTURES_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    console.log(error);
                    dispatch({
                        type: Type.SET_CULTURES_FAILED
                    });
                });
        });
    }
}
