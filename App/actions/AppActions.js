import axios from 'axios';
import { AsyncStorage } from 'react-native';

export const Type = {
    LOGIN_REST_CALL: 'LOGIN_REST_CALL',
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGIN_FAILED: 'LOGIN_FAILED',
    SET_LOGIN_FAILED_TO_FALSE_AGAIN: 'SET_LOGIN_FAILED_TO_FALSE_AGAIN',
    LOGOUT: 'LOGOUT',

    SET_INITIAL_STATE: 'SET_INITIAL_STATE',
    SET_TOKEN: 'SET_TOKEN',

    GET_WORK_ORDERS_CALL: 'GET_WORK_ORDERS_CALL',
    GET_WORK_ORDERS_SUCCESS: 'GET_WORK_ORDERS_SUCCESS',
    GET_WORK_ORDERS_FAILED: 'GET_WORK_ORDERS_FAILED',
    GET_WORK_ORDER_BY_ID_CALL: 'GET_WORK_ORDER_BY_ID_CALL',
    GET_WORK_ORDER_BY_ID_SUCCESS: 'GET_WORK_ORDER_BY_ID_SUCCESS',
    GET_WORK_ORDER_BY_ID_FAILED: 'GET_WORK_ORDER_BY_ID_FAILED',
    SEND_WORK_ORDER_CALL: 'SEND_WORK_ORDER_CALL',
    SEND_WORK_ORDER_SUCCESS: 'SEND_WORK_ORDER_SUCCESS',
    SEND_WORK_ORDER_FAILED: 'SEND_WORK_ORDER_FAILED',
    SET_FOR_WORK_ORDER_SUCCESS_TO_FALSE_AGAIN: 'SET_FOR_WORK_ORDER_SUCCESS_TO_FALSE_AGAIN',
    SET_FOR_WORK_ORDER_FAILED_TO_FALSE_AGAIN: 'SET_FOR_WORK_ORDER_FAILED_TO_FALSE_AGAIN',

    GET_WORKING_OPERATIONS_GROUPS_CALL: 'GET_WORKING_OPERATIONS_GROUPS_CALL',
    GET_WORKING_OPERATIONS_GROUPS_SUCCESS: 'GET_WORKING_OPERATIONS_GROUPS_SUCCESS',
    GET_WORKING_OPERATIONS_GROUPS_FAILED: 'GET_WORKING_OPERATIONS_GROUPS_FAILED',

    SET_WORKING_OPERATIONS_GROUP_ID_FOR_FETCH: 'SET_WORKING_OPERATIONS_GROUP_ID_FOR_FETCH',
    GET_WORKING_OPERATIONS_WITH_GROUP_ID_CALL: 'GET_WORKING_OPERATIONS_WITH_GROUP_ID_CALL',
    GET_WORKING_OPERATIONS_WITH_GROUP_ID_SUCCESS: 'GET_WORKING_OPERATIONS_WITH_GROUP_ID_SUCCESS',
    GET_WORKING_OPERATIONS_WITH_GROUP_ID_FAILED: 'GET_WORKING_OPERATIONS_WITH_GROUP_ID_FAILED',

    SET_PROPERTY_FOR_WORKING_OPERATION: 'SET_PROPERTY_FOR_WORKING_OPERATION',
    SET_WORK_ORDER_START_TIME: 'SET_WORK_ORDER_START_TIME',

    GET_WORK_ORDER_TABLES_SUCCESS: 'GET_WORK_ORDER_TABLES_SUCCESS',
    GET_WORK_ORDER_TABLES_CALL: 'GET_WORK_ORDER_TABLES_CALL',
    GET_WORK_ORDER_TABLES_FAILED: 'GET_WORK_ORDER_TABLES_FAILED',

    SET_SELECTED_TABLE: 'SET_SELECTED_TABLE',
    SET_SELECTED_TABLES_FROM_WORK_ORDER: 'SET_SELECTED_TABLES_FROM_WORK_ORDER',
    REMOVE_SELECTED_TABLE: 'REMOVE_SELECTED_TABLE',
    SET_TABLES_FOR_WORK_ORDER: 'SET_TABLES_FOR_WORK_ORDER',

    // MACHINES
    SET_SELECTED_MACHINE: 'SET_SELECTED_MACHINE',
    SET_SELECTED_MACHINES_FROM_WORK_ORDER: 'SET_SELECTED_MACHINES_FROM_WORK_ORDER',
    REMOVE_SELECTED_MACHINE: 'REMOVE_SELECTED_MACHINE',
    SET_MACHINES_FOR_WORK_ORDER: 'SET_MACHINES_FOR_WORK_ORDER',

    GET_MACHINES_GROUPS_CALL: 'GET_MACHINES_GROUPS_CALL',
    GET_MACHINES_GROUPS_SUCCESS: 'GET_MACHINES_GROUPS_SUCCESS',
    GET_MACHINES_GROUPS_FAILED: 'GET_MACHINES_GROUPS_FAILED',

    GET_MACHINES_SUBGROUPS_CALL: 'GET_MACHINES_SUBGROUPS_CALL',
    GET_MACHINES_SUBGROUPS_SUCCESS: 'GET_MACHINES_SUBGROUPS_SUCCESS',
    GET_MACHINES_SUBGROUPS_FAILED: 'GET_MACHINES_SUBGROUPS_FAILED',

    GET_MACHINES_CALL: 'GET_MACHINES_CALL',
    GET_MACHINES_SUCCESS: 'GET_MACHINES_SUCCESS',
    GET_MACHINES_FAILED: 'GET_MACHINES_FAILED',

    // MATERIALS
    GET_WORK_ORDER_MATERIALS_GROUPS_CALL: 'GET_WORK_ORDER_MATERIALS_GROUPS_CALL',
    GET_WORK_ORDER_MATERIALS_GROUPS_SUCCESS: 'GET_WORK_ORDER_MATERIALS_GROUPS_SUCCESS',
    GET_WORK_ORDER_MATERIALS_GROUPS_FAILED: 'GET_WORK_ORDER_MATERIALS_GROUPS_FAILED',

    GET_MATERIALS_SUBGROUPS_CALL: 'GET_MATERIALS_SUBGROUPS_CALL',
    GET_MATERIALS_SUBGROUPS_SUCCESS: 'GET_MATERIALS_SUBGROUPS_SUCCESS',
    GET_MATERIALS_SUBGROUPS_FAILED: 'GET_MATERIALS_SUBGROUPS_FAILED',

    GET_MATERIALS_CALL: 'GET_MATERIALS_CALL',
    GET_MATERIALS_SUCCESS: 'GET_MATERIALS_SUCCESS',
    GET_MATERIALS_FAILED: 'GET_MATERIALS_FAILED',

    SET_CHOOSEN_MATERIAL: 'SET_CHOOSEN_MATERIAL',
    REMOVE_SELECTED_MATERIAL: 'REMOVE_SELECTED_MATERIAL',
    SET_HELP_MATERIALS_FOR_WORK_ORDER: 'SET_HELP_MATERIALS_FOR_WORK_ORDER',

    SET_MATERIALS_FROM_WORK_ORDER: 'SET_MATERIALS_FROM_WORK_ORDER',

    // HELP MACHINES
    SET_SELECTED_HELP_MACHINE: 'SET_SELECTED_HELP_MACHINE',
    SET_SELECTED_HELP_MACHINES_FROM_WORK_ORDER: 'SET_SELECTED_HELP_MACHINES_FROM_WORK_ORDER',
    REMOVE_SELECTED_HELP_MACHINE: 'REMOVE_SELECTED_HELP_MACHINE',
    SET_HELP_MACHINES_FOR_WORK_ORDER: 'SET_HELP_MACHINES_FOR_WORK_ORDER',
    GET_HELP_MACHINES_GROUPS_CALL: 'GET_HELP_MACHINES_GROUPS_CALL',
    GET_HELP_MACHINES_GROUPS_SUCCESS: 'GET_HELP_MACHINES_GROUPS_SUCCESS',
    GET_HELP_MACHINES_GROUPS_FAILED: 'GET_HELP_MACHINES_GROUPS_FAILED',

    GET_HELP_MACHINES_CALL: 'GET_HELP_MACHINES_CALL',
    GET_HELP_MACHINES_SUCCESS: 'GET_HELP_MACHINES_SUCCESS',
    GET_HELP_MACHINES_FAILED: 'GET_HELP_MACHINES_FAILED',

    SET_STATUS: 'SET_STATUS'

};


export function setTokenInReducer(token) {
    return (dispatch) => {
        dispatch({
            type: Type.SET_TOKEN,
            data: token});
    }
}

export function setInitialState(component) {
    return (dispatch) => {
        dispatch({
            type: Type.SET_INITIAL_STATE,
            data: component
            });
    }
}


export function setStartTimeForWorkingOrder(pocetak, zavrsetak) {
    if (!zavrsetak) {
        zavrsetak = pocetak;
    }
    var dataOperation = {
        datum_pocetka: pocetak,
        datum_zavrsetka: zavrsetak
    };
    return (dispatch) => {
        dispatch({
            type: Type.SET_WORK_ORDER_START_TIME,
            data: dataOperation
        });
    }
}

export function loginRestCall(username, password) {
    return (dispatch) => {

        dispatch({
            type: Type.LOGIN_REST_CALL
        });

        axios.post('http://agrolife.greensoft.co/login', {
            username: username,
            password: password
        })
        .then(function (response) {
            dispatch({
                type: Type.LOGIN_SUCCESS,
                data: response.data
            });
        })
        .catch(function (error) {
            console.log('error');
            dispatch({
                type: Type.LOGIN_FAILED
            });
        });
    }
}

export function getWorkOrders() {
    return (dispatch) => {
        dispatch({
            type: Type.GET_WORK_ORDERS_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            let config = {
                headers: {'Authorization': 'Bearer [' + token +']'}
               };
            return config;

        }).then((config) => {
           return axios.get('http://agrolife.greensoft.co/workOrder/getAllInList', config)
                       .then(function (response) {
                            dispatch({
                                type: Type.GET_WORK_ORDERS_SUCCESS,
                                data: response.data
                            });
                        })
                       .catch(function (error) {
                            dispatch({
                                type: Type.GET_WORK_ORDERS_FAILED
                            });
                       });
        });




    }
}

export function getWorkOrderById(id) {
    return (dispatch) => {
        dispatch({
            type: Type.GET_WORK_ORDER_BY_ID_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            var config = {
                headers: {'Authorization': 'Bearer [' + token +']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/workOrder/getById?id=' + id, config)
                .then(function (response) {
                     dispatch({
                        type: Type.GET_WORK_ORDER_BY_ID_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_WORK_ORDER_BY_ID_FAILED
                    });
                });
        });




    }
}

export function sendWorkOrder(workOrder) {

     var updateOrCreate = 'fullCreate';
     if(workOrder.id){
       updateOrCreate = 'fullUpdate';
     }
    return (dispatch) => {
        dispatch({
            type: Type.SEND_WORK_ORDER_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            var config = {
                headers: {'Authorization': 'Bearer [' + token +']'}
            };
            return config;

        }).then((config) => {
            return axios.post('http://agrolife.greensoft.co/workOrder/' + updateOrCreate,
                {
                 radniNalog: workOrder
                },
                config)
                .then(function (response) {
                    dispatch({
                        type: Type.SEND_WORK_ORDER_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.SEND_WORK_ORDER_FAILED
                    });
                });
        });




    }
}





export function getWorkingOperationsGroups() {
    return (dispatch) => {
        dispatch({
            type: Type.GET_WORKING_OPERATIONS_GROUPS_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            var config = {
                headers: {'Authorization': 'Bearer [' + token +']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/workingOperation/getGroups', config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_WORKING_OPERATIONS_GROUPS_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_WORKING_OPERATIONS_GROUPS_FAILED
                    });
                });
        });




    }
}

export function setWorkingOperationsGroupId(id_radna_operacija_grupa) {
    console.log('setWorkingOperationsGroupId action: id: ' + id_radna_operacija_grupa);
    return (dispatch) => {
        dispatch({
            type: Type.SET_WORKING_OPERATIONS_GROUP_ID_FOR_FETCH,
            data: id_radna_operacija_grupa
        });
    }
}

export function getWorkingOperationsWithGroupId(id_radna_operacija_grupa) {
    console.log('getWorkingOperationsWithGroupId id_radna_operacija_grupa:  ' + id_radna_operacija_grupa);
    return (dispatch) => {
        dispatch({
            type: Type.GET_WORKING_OPERATIONS_WITH_GROUP_ID_CALL
        });


       return AsyncStorage.getItem("token").then((token) => {
            var config = {
                headers: {'Authorization': 'Bearer [' + token +']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/workingOperation/getByGroup?id_radna_operacija_grupa=' + id_radna_operacija_grupa, config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_WORKING_OPERATIONS_WITH_GROUP_ID_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    console.log('getWorkingOperationsWithGroupId error:  ' + JSON.stringify(error));
                    dispatch({
                        type: Type.GET_WORKING_OPERATIONS_WITH_GROUP_ID_FAILED
                    });
                });
        });
    }
}


export function getTables() {

    return (dispatch) => {
            dispatch({
                type: Type.GET_WORK_ORDER_TABLES_CALL
            });

            AsyncStorage.getItem("token").then((token) => {
                var config = {
                    headers: {'Authorization': 'Bearer [' + token + ']'}
                };
                return config;

            }).then((config) => {
                return axios.get('http://agrolife.greensoft.co/parcel/getParcelsWithRelation', config)
                    .then(function (response) {
                        dispatch({
                            type: Type.GET_WORK_ORDER_TABLES_SUCCESS,
                            data: response.data
                        });
                    })
                    .catch(function (error) {
                        dispatch({
                            type: Type.GET_WORK_ORDER_TABLES_FAILED
                        });
                    });
            });
        }
}


export function setWorkingOperationForWorkOrder(id, naziv) {
    var dataOperation = {id_radna_operacija: id, naziv_radne_operacije: naziv};
    return (dispatch) => {
        dispatch({
            type: Type.SET_PROPERTY_FOR_WORKING_OPERATION,
            data: dataOperation
        });
    }
}

export function setSelectedTable(selectedTable) {
    return (dispatch) => {
        dispatch({
            type: Type.SET_SELECTED_TABLE,
            data: selectedTable
        });
    }
}

export function removeSelectedTable(selectedTable) {
    var tableToRemove = selectedTable;
    return (dispatch) => {
        dispatch({
            type: Type.REMOVE_SELECTED_TABLE,
            data: tableToRemove
        });
    }
}

export function setTablesForWorkOrder() {
    return (dispatch) => {
        dispatch({
            type: Type.SET_TABLES_FOR_WORK_ORDER
        });
    }
}



export function getMachinesGroups() {
    return (dispatch) => {
        dispatch({
            type: Type.GET_MACHINES_GROUPS_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            var config = {
                headers: {'Authorization': 'Bearer [' + token +']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/machine/getGroups', config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_MACHINES_GROUPS_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_MACHINES_GROUPS_FAILED
                    });
                });
        });




    }
}

export function getMachinesSubgroups(id_pogonska_masina_grupa) {
    return (dispatch) => {
        dispatch({
            type: Type.GET_MACHINES_SUBGROUPS_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            var config = {
                headers: {'Authorization': 'Bearer [' + token +']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/machine/getSubgroups?id_pogonska_masina_grupa=' + id_pogonska_masina_grupa, config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_MACHINES_SUBGROUPS_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_MACHINES_SUBGROUPS_FAILED
                    });
                });
        });
    }
}

export function getMachines(id_pogonska_masina_grupa, id_pogonska_masina_podgrupa) {
    return (dispatch) => {
        dispatch({
            type: Type.GET_MACHINES_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            var config = {
                headers: {'Authorization': 'Bearer [' + token +']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/machine/getByGroupAndSubgroup?id_pogonska_masina_grupa=' + id_pogonska_masina_grupa + '&id_pogonska_masina_podgrupa=' + id_pogonska_masina_podgrupa, config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_MACHINES_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_MACHINES_FAILED
                    });
                });
        });




    }
}

export function setSelectedMachine(selectedMachine) {
    return (dispatch) => {
        dispatch({
            type: Type.SET_SELECTED_MACHINE,
            data: selectedMachine
        });
    }
}

export function removeSelectedMachine(selectedMachine) {
    return (dispatch) => {
        dispatch({
            type: Type.REMOVE_SELECTED_MACHINE,
            data: selectedMachine
        });
    }
}

export function setMachinesForWorkOrder() {
    return (dispatch) => {
        dispatch({
            type: Type.SET_MACHINES_FOR_WORK_ORDER});
    }
}


export function getMaterialsGroups() {
    return (dispatch) => {
        dispatch({
            type: Type.GET_WORK_ORDER_MATERIALS_GROUPS_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            console.log('getWorkingOperationsGroups token:  ' + token);
            var config = {
                headers: {'Authorization': 'Bearer [' + token + ']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/material/getGroups', config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_WORK_ORDER_MATERIALS_GROUPS_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_WORK_ORDER_MATERIALS_GROUPS_FAILED
                    });
                });
        });
    }
}

export function getMaterialsSubGroups(id) {
    return (dispatch) => {
        dispatch({
            type: Type.GET_MATERIALS_SUBGROUPS_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            var config = {
                headers: {'Authorization': 'Bearer [' + token + ']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/material/getSubgroups?id_materijal_grupa=' + id, config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_MATERIALS_SUBGROUPS_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_MATERIALS_SUBGROUPS_FAILED
                    });
                });
        });
    }
}

export function getMaterials(idGroup, idSubgroup) {
    console.log('cisto ovako : ' + idGroup + ' ---' + idSubgroup);
    return (dispatch) => {
        dispatch({
            type: Type.GET_MATERIALS_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            var config = {
                headers: {'Authorization': 'Bearer [' + token + ']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/material/getByGroupAndSubgroup?id_materijal_grupa=' + idSubgroup + ' &id_materijal_podgrupa=' + idGroup, config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_MATERIALS_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_MATERIALS_FAILED
                    });
                });
        });
    }
}


export function setChoosenMaterials(materialToOrder) {
    return (dispatch) => {
        dispatch({
            type: Type.SET_CHOOSEN_MATERIAL,
            data: materialToOrder
        });
    }
}

export function removeSelectedMaterial(material) {
    return (dispatch) => {
        dispatch({
            type: Type.REMOVE_SELECTED_MATERIAL,
            data: material
        });
    }
}


export function setMaterialsForWorkOrder() {
    return (dispatch) => {
        dispatch({
            type: Type.SET_HELP_MATERIALS_FOR_WORK_ORDER
        });
    }
}

export function setMaterialsFromWorkOrder() {
    return (dispatch) => {
        dispatch({
            type: Type.SET_MATERIALS_FROM_WORK_ORDER
        });
    }
}


export function getHelpMachinesGroups() {
    return (dispatch) => {
        dispatch({
            type: Type.GET_HELP_MACHINES_GROUPS_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            var config = {
                headers: {'Authorization': 'Bearer [' + token +']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/helpMachine/getGroups', config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_HELP_MACHINES_GROUPS_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_HELP_MACHINES_GROUPS_FAILED
                    });
                });
        });




    }
}



export function getHelpMachines(id_prikljucna_masina_grupa) {
    return (dispatch) => {
        dispatch({
            type: Type.GET_HELP_MACHINES_CALL
        });


        AsyncStorage.getItem("token").then((token) => {
            var config = {
                headers: {'Authorization': 'Bearer [' + token +']'}
            };
            return config;

        }).then((config) => {
            return axios.get('http://agrolife.greensoft.co/helpMachine/getByGroup?id_prikljucna_masina_grupa=' + id_prikljucna_masina_grupa, config)
                .then(function (response) {
                    dispatch({
                        type: Type.GET_HELP_MACHINES_SUCCESS,
                        data: response.data
                    });
                })
                .catch(function (error) {
                    dispatch({
                        type: Type.GET_HELP_MACHINES_FAILED
                    });
                });
        });




    }
}

export function setSelectedHelpMachine(selectedHelpMachine) {
    return (dispatch) => {
        dispatch({
            type: Type.SET_SELECTED_HELP_MACHINE,
            data: selectedHelpMachine
        });
    }
}

export function removeSelectedHelpMachine(selectedHelpMachine) {
    return (dispatch) => {
        dispatch({
            type: Type.REMOVE_SELECTED_HELP_MACHINE,
            data: selectedHelpMachine
        });
    }
}

export function setHelpMachinesForWorkOrder() {
    return (dispatch) => {
        dispatch({
            type: Type.SET_HELP_MACHINES_FOR_WORK_ORDER});
    }
}

export function setSelectedHelpMachinesFromWorkOrder() {
    return (dispatch) => {
        dispatch({
            type: Type.SET_SELECTED_HELP_MACHINES_FROM_WORK_ORDER});
    }
}

export function setSelectedMachinesFromWorkOrder() {
    return (dispatch) => {
        dispatch({
            type: Type.SET_SELECTED_MACHINES_FROM_WORK_ORDER});
    }
}

export function setSelectedTablesFromWorkOrder() {
    return (dispatch) => {
        dispatch({
            type: Type.SET_SELECTED_TABLES_FROM_WORK_ORDER});
    }
}

export function setWorkOrderSuccessToFalseAgain() {
    return (dispatch) => {
        dispatch({
            type: Type.SET_FOR_WORK_ORDER_SUCCESS_TO_FALSE_AGAIN});
    }
}

export function setLoginFailedToFalseAgain() {
    return (dispatch) => {
        dispatch({
            type: Type.SET_LOGIN_FAILED_TO_FALSE_AGAIN});
    }
}



export function setWorkOrderFailedToFalseAgain() {
    return (dispatch) => {
        dispatch({
            type: Type.SET_FOR_WORK_ORDER_FAILED_TO_FALSE_AGAIN});
    }
}

export function setStatus(status) {
    return (dispatch) => {
        dispatch({
            type: Type.SET_STATUS,
            status: status
        })
    }
}

export function logout() {
    return (dispatch) => {
        dispatch({
            type: Type.LOGOUT
        });
    }
}







