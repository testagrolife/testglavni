export const Type = {
    CHANGE_CONNECTION_STATUS: 'CHANGE_CONNECTION_STATUS'
};

export function changeConnectionStatus(status) {
    return (dispatch) => {
        dispatch({
            type: Type.CHANGE_CONNECTION_STATUS,
            status: status
        });
    }
}